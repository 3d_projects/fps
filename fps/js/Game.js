Game = function(canvasId,playerConfig ,props) {
    var canvas = document.getElementById(canvasId);
    var engine = new BABYLON.Engine(canvas, true);
    this.engine = engine;
    var _this = this;
    _this.actualTime = Date.now();

    this.allSpawnPoints = [
        new BABYLON.Vector3(-20,5,40),
        new BABYLON.Vector3(20,5,40),
        new BABYLON.Vector3(-20,5,-40),
        new BABYLON.Vector3(20,5,-40)
    ];

    this.scene = this._initScene(engine);

    var armory = new Armory(this);
    _this.armory = armory;
    
    var _player = new Player(_this, playerConfig.name , canvas);

    this._PlayerData = _player;

    var _arena = new Arena(_this,props);
    this._ArenaData = _arena;

    this._rockets = [];

    this._explosionRadius = [];

    this._lasers = [];

    engine.runRenderLoop(function () {

		//FPS
        _this.fps = Math.round(1000/engine.getDeltaTime());

        _player._checkMove((_this.fps)/60);

        _this.renderRockets();
        _this.renderExplosionRadius();

        _this.renderLaser();

        _this.renderWeapons();

        _this._ArenaData._checkProps();
        
		setTimeout(function() {
			_this.scene.render();
		}, 500);
		
        if(_player.camera.weapons.launchBullets === true){
            _player.camera.weapons.launchFire();
        }
    });

    window.addEventListener("resize", function () {
        if (engine) {
            engine.resize();
        }
    },false);

};


Game.prototype = {
    _initScene : function(engine) {
        var scene = new BABYLON.Scene(engine);
        scene.clearColor = new BABYLON.Color3(0.31, 0.48, 0.64);
        scene.ambientColor = new BABYLON.Color3(0.37, 0.39, 0.39);
        scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
        scene.collisionsEnabled = true;
        scene.debugLayer.show();
        return scene;
    },
    renderRockets : function() {
        for (var i = 0; i < this._rockets.length; i++) {

		var paramsRocket = this._rockets[i].paramsRocket;
            var paramsBlast = paramsRocket.ammos;

            var rayRocket = new BABYLON.Ray(this._rockets[i].position,this._rockets[i].direction);

            var meshFound = this._rockets[i].getScene().pickWithRay(rayRocket);

            if(!meshFound || meshFound.distance < 10){
                if(meshFound.pickedMesh && !meshFound.pickedMesh.isMain){
                    var explosionRadius = BABYLON.Mesh.CreateSphere("sphere", 5.0, paramsBlast.explosionRadius/2, this.scene, false, BABYLON.Mesh.DOUBLESIDE);
                    explosionRadius.position = meshFound.pickedPoint;
                    explosionRadius.isPickable = false;
                    explosionRadius.material = new BABYLON.StandardMaterial("textureExplosion", this.scene);
                    explosionRadius.material.diffuseColor = new BABYLON.Color3(1,0.2,0);
                    explosionRadius.material.emissiveColor = new BABYLON.Color3(1,0.2,0);
                    explosionRadius.material.specularColor = new BABYLON.Color3(0,0,0);
                    explosionRadius.material.alpha = 0.8;

                    explosionRadius.computeWorldMatrix(true);

                    if (this._PlayerData.isAlive && this._PlayerData.camera.playerBox && explosionRadius.intersectsMesh(this._PlayerData.camera.playerBox)) {

					if(this._rockets[i].owner){
                            var whoDamage = this._rockets[i].owner;
                        }else{
                            var whoDamage = false;
                        }

                        this._PlayerData.getDamage(paramsRocket.damage,whoDamage);
                    }
                    this._explosionRadius.push(explosionRadius);
                }
                this._rockets[i].dispose();
                this._rockets.splice(i,1);
            }else{
                let relativeSpeed = paramsRocket.ammos.rocketSpeed / ((this.fps)/60);
                this._rockets[i].position.addInPlace(this._rockets[i].direction.scale(relativeSpeed))
            }
        };
    },
    renderExplosionRadius : function(){
        if(this._explosionRadius.length > 0){
            for (var i = 0; i < this._explosionRadius.length; i++) {
                this._explosionRadius[i].material.alpha -= 0.02;
                if(this._explosionRadius[i].material.alpha<=0){
                    this._explosionRadius[i].dispose();
                    this._explosionRadius.splice(i, 1);
                }
            }
        }
    },
    renderLaser : function(){
        if(this._lasers.length > 0){
            for (var i = 0; i < this._lasers.length; i++) {
                this._lasers[i].edgesWidth -= 0.5;
                if(this._lasers[i].edgesWidth<=0){
                    this._lasers[i].dispose();
                    this._lasers.splice(i, 1);
                }
            }
        }
    },
    renderWeapons : function(){
        if(this._PlayerData && this._PlayerData.camera.weapons.inventory){
            var inventoryWeapons = this._PlayerData.camera.weapons.inventory;
            
            for (var i = 0; i < inventoryWeapons.length; i++) {
                if(inventoryWeapons[i].isActive && inventoryWeapons[i].position.y < this._PlayerData.camera.weapons.topPositionY){
                    inventoryWeapons[i].position.y += 0.1;
                }else if(!inventoryWeapons[i].isActive && inventoryWeapons[i].position.y != this._PlayerData.camera.weapons.bottomPosition.y){
                    inventoryWeapons[i].position.y -= 0.1;
                }
            }
        }
    },
    createGhostRocket : function(dataRocket) {
        var positionRocket = dataRocket[0];
        var rotationRocket = dataRocket[1];
        var directionRocket = dataRocket[2];
        var idPlayer = dataRocket[3];

        newRocket = BABYLON.Mesh.CreateBox('rocket', 0.5, this.scene);
        
        newRocket.scaling = new BABYLON.Vector3(1,0.7,2);

        newRocket.direction = new BABYLON.Vector3(directionRocket.x,directionRocket.y,directionRocket.z);

        newRocket.position = new BABYLON.Vector3(
            positionRocket.x + (newRocket.direction.x * 1) , 
            positionRocket.y + (newRocket.direction.y * 1) ,
            positionRocket.z + (newRocket.direction.z * 1));
        newRocket.rotation = new BABYLON.Vector3(rotationRocket.x,rotationRocket.y,rotationRocket.z);

        newRocket.scaling = new BABYLON.Vector3(0.5,0.5,1);
        newRocket.isPickable = false;
        newRocket.owner = idPlayer;

        newRocket.material = new BABYLON.StandardMaterial("textureWeapon", this.scene, false, BABYLON.Mesh.DOUBLESIDE);
        newRocket.material.diffuseColor = this.armory.weapons[2].setup.colorMesh;
        newRocket.paramsRocket = this.armory.weapons[2].setup;
        
        game._rockets.push(newRocket);
    },
    createGhostLaser : function(dataRocket){
        var position1 = dataRocket[0];
        var position2 = dataRocket[1];
        var idPlayer = dataRocket[2];

        let line = BABYLON.Mesh.CreateLines("lines", [
                    position1,
                    position2
                ], this.scene);
        var colorLine = new BABYLON.Color3(Math.random(), Math.random(), Math.random());
        line.color = colorLine;
        line.enableEdgesRendering();
        line.isPickable = false;
        line.edgesWidth = 40.0;
        line.edgesColor = new BABYLON.Color4(colorLine.r, colorLine.g, colorLine.b, 1);
        this._lasers.push(line);
    },
    displayScore(room){
        if(room.length>=5){
            var limitLoop = 4;
        }else{
            var limitLoop = room.length-1;
        }
        var indexName = 0;
        for (var i = 0; i <= limitLoop ; i++) {
            document.getElementById('player'+indexName).innerText = room[i].name;
            document.getElementById('scorePlayer'+indexName).innerText = room[i].score;
            indexName++;
        }
    }
};

function degToRad(deg)
{
   return (Math.PI*deg)/180
}
// ----------------------------------------------------

function radToDeg(rad)
{
   // return (Math.PI*deg)/180
   return (rad*180)/Math.PI
}
// ----------------------------------------------------