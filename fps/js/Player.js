Player = function(game, name, canvas) {
    var _this = this;

    this.name = name;
    this.weponShoot = false;

    this.ghostPlayers=[];

    this.game = game;

    this.speed = 0.7;
    // this.speed = 2;

    this.angularSensibility = 200;

    this.killStreak = 0;

    this.displayAnnouncement = document.getElementById('announcementKill');
    this.textDisplayAnnouncement = document.getElementById('textAnouncement');

    this.axisMovement = [false,false,false,false];

    this.textHealth = document.getElementById('textHealth');
    this.textArmor = document.getElementById('textArmor');

    window.addEventListener("keyup", function(evt) {
        if(evt.keyCode == 90 || evt.keyCode == 83 || evt.keyCode == 81 || evt.keyCode == 68 ){
            switch(evt.keyCode){
                case 90:
                _this.camera.axisMovement[0] = false;
                break;
                case 83:
                _this.camera.axisMovement[1] = false;
                break;
                case 81:
                _this.camera.axisMovement[2] = false;
                break;
                case 68:
                _this.camera.axisMovement[3] = false;
                break;
            }
            var data={
                axisMovement : _this.camera.axisMovement
            };
            _this.sendNewData(data)
            
        }
    }, false);

    window.addEventListener("keydown", function(evt) {
        if(evt.keyCode == 90 || evt.keyCode == 83 || evt.keyCode == 81 || evt.keyCode == 68 ){
            switch(evt.keyCode){
                case 90:
                _this.camera.axisMovement[0] = true;
                break;
                case 83:
                _this.camera.axisMovement[1] = true;
                break;
                case 81:
                _this.camera.axisMovement[2] = true;
                break;
                case 68:
                _this.camera.axisMovement[3] = true;
                break;
                // case 13:
                //     _this.newDeadEnnemy();
                // break;
            }
            var data={
                axisMovement : _this.camera.axisMovement
            };
            _this.sendNewData(data)
        }
        
    }, false);

    window.addEventListener("mousemove", function(evt) {
        if(_this.rotEngaged === true){
            _this.camera.playerBox.rotation.y+=evt.movementX * 0.001 * (_this.angularSensibility / 250);
            var nextRotationX = _this.camera.playerBox.rotation.x + (evt.movementY * 0.001 * (_this.angularSensibility / 250));
            if( nextRotationX < degToRad(90) && nextRotationX > degToRad(-90)){
                _this.camera.playerBox.rotation.x+=evt.movementY * 0.001 * (_this.angularSensibility / 250);
            }
            var data={
                rotation : _this.camera.playerBox.rotation
            };
            _this.sendNewData(data)
        }
    }, false);

    var canvas = this.game.scene.getEngine().getRenderingCanvas();

   canvas.addEventListener("mousedown", function(evt) {
        if (_this.controlEnabled && !_this.weponShoot) {
            _this.weponShoot = true;
            _this.handleUserMouseDown();
        }
    }, false);

    canvas.addEventListener("mouseup", function(evt) {
        if (_this.controlEnabled && _this.weponShoot) {
            _this.weponShoot = false;
            _this.handleUserMouseUp();
        }
    }, false);

    this.previousWheeling = 0;

    canvas.addEventListener("mousewheel", function(evt) {

        if(Math.round(evt.timeStamp - _this.previousWheeling)>10){
            if(evt.deltaY<0){
                // scroll vers le haut
                _this.camera.weapons.nextWeapon(1);
            }else{
                // scroll vers le bas
                _this.camera.weapons.nextWeapon(-1);
            }
            _this.previousWheeling = evt.timeStamp;
        }
        
    }, false);

    
    this._initCamera(this.game.scene, canvas); 

    this.controlEnabled = false;

    this._initPointerLock(); 

    this.textHealth.innerText = this.camera.health;
    this.textArmor.innerText = this.camera.armor;

    _this.camera.canJump = true;

    _this.jumpHeight = 10;

    _this.originHeight = _this.camera.playerBox.position.clone();
    window.addEventListener("keypress", function(evt) {
        if(evt.keyCode === 32){
            if(_this.camera.canJump===true){
                _this.camera.jumpNeed = _this.camera.playerBox.position.y + _this.jumpHeight;

                _this.camera.canJump=false;
                var data={
                    jumpNeed : _this.camera.jumpNeed
                };
                _this.sendNewData(data)
            }
        }
    }, false);
};

Player.prototype = {
    _initCamera : function(scene, canvas) {
        var randomPoint = Math.random();

        randomPoint = Math.round(randomPoint * (this.game.allSpawnPoints.length - 1));

        this.spawnPoint = this.game.allSpawnPoints[randomPoint];

		var mat = new BABYLON.StandardMaterial("mat", scene);
        mat.diffuseTexture = new BABYLON.Texture("../assets/images/asalways.jpg", scene);
	
        var playerBox = BABYLON.Mesh.CreateBox("headMainPlayer", 1, scene);
        playerBox.position = this.spawnPoint.clone();
        playerBox.position.y = 10;
        playerBox.ellipsoid = new BABYLON.Vector3(0.5, 4, 0.5);
        playerBox.isPickable = false;
		playerBox.material = mat;

        this.camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(0, 0, 0), scene);
        CAMERA = this.camera;
        this.camera.fov = 1.3;
        this.camera.playerBox = playerBox
        this.camera.parent = this.camera.playerBox;

        this.camera.playerBox.checkCollisions = true;
        this.camera.playerBox.applyGravity = true;

        this.isAlive = true;

        this.camera.health = 100;
        this.camera.isMain = true;
        this.camera.armor = 0;

        this.camera.weapons = new Weapons(this);

        this.camera.axisMovement = [false,false,false,false];

        this.camera.canJump = true;
        this.game.scene.activeCamera = this.camera;

        var hitBoxPlayer = BABYLON.Mesh.CreateBox("hitBoxPlayer", 1, scene);
        hitBoxPlayer.parent = this.camera.playerBox;
        hitBoxPlayer.scaling.y = 1;
        hitBoxPlayer.isPickable = true;


    },
    handleUserMouseDown : function() {
        if(this.isAlive === true){
            this.camera.weapons.fire();
        }
    },
    handleUserMouseUp : function() {
        if(this.isAlive === true){
            this.camera.weapons.stopFire();
        }
    },
    _initPointerLock : function() {
        var _this = this;
        
        var canvas = this.game.scene.getEngine().getRenderingCanvas();
        canvas.addEventListener("click", function(evt) {
            canvas.requestPointerLock = canvas.requestPointerLock || canvas.msRequestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
            if (canvas.requestPointerLock) {
                canvas.requestPointerLock();
            }
        }, false);

        var pointerlockchange = function (event) {
            _this.controlEnabled = (document.mozPointerLockElement === canvas || document.webkitPointerLockElement === canvas || document.msPointerLockElement === canvas || document.pointerLockElement === canvas);
            if (!_this.controlEnabled) {
                _this.rotEngaged = false;
            } else {
                _this.rotEngaged = true;
            }
        };
        
        document.addEventListener("pointerlockchange", pointerlockchange, false);
        document.addEventListener("mspointerlockchange", pointerlockchange, false);
        document.addEventListener("mozpointerlockchange", pointerlockchange, false);
        document.addEventListener("webkitpointerlockchange", pointerlockchange, false);
    },
    _checkMove : function(ratioFps){
        this._checkUniqueMove(ratioFps,this.camera);
        for (var i = 0; i < this.ghostPlayers.length; i++) {
            this._checkUniqueMove(ratioFps,this.ghostPlayers[i]);
        }
    },
    _checkUniqueMove : function(ratioFps, player) {
        var relativeSpeed = this.speed / ratioFps;
        var playerSelected = player
       
	   // check ghost ou non (seul les ghost on un head)
        if(playerSelected.head){
            var rotationPoint = playerSelected.head.rotation;
        }else{
            var rotationPoint = playerSelected.playerBox.rotation;
        }
        if(playerSelected.axisMovement[0]){
            forward = new BABYLON.Vector3(
                parseFloat(Math.sin(parseFloat(rotationPoint.y))) * relativeSpeed/1.4, 
                0, 
                parseFloat(Math.cos(parseFloat(rotationPoint.y))) * relativeSpeed/1.4
            );
            playerSelected.playerBox.moveWithCollisions(forward);
        }
        if(playerSelected.axisMovement[1]){
            backward = new BABYLON.Vector3(
                parseFloat(-Math.sin(parseFloat(rotationPoint.y))) * relativeSpeed/1.4, 
                0, 
                parseFloat(-Math.cos(parseFloat(rotationPoint.y))) * relativeSpeed/1.4
            );

            playerSelected.playerBox.moveWithCollisions(backward);
        }
        if(playerSelected.axisMovement[2]){
            left = new BABYLON.Vector3(
                parseFloat(Math.sin(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed/1.4, 
                0, 
                parseFloat(Math.cos(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed/1.4
            );
            playerSelected.playerBox.moveWithCollisions(left);
        }
        if(playerSelected.axisMovement[3]){
            right = new BABYLON.Vector3(
                parseFloat(-Math.sin(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed/1.4, 
                0, 
                parseFloat(-Math.cos(parseFloat(rotationPoint.y) + degToRad(-90))) * relativeSpeed/1.4
            );
            playerSelected.playerBox.moveWithCollisions(right);
        }
        if(playerSelected.jumpNeed){

		percentMove = playerSelected.jumpNeed - playerSelected.playerBox.position.y;
            // Axe de mouvement
            up = new BABYLON.Vector3(0,percentMove/4 *  relativeSpeed,0);
            playerSelected.playerBox.moveWithCollisions(up);
            if(playerSelected.playerBox.position.y + 1 > playerSelected.jumpNeed || this.game._ArenaData.border.intersectsMesh(playerSelected.playerBox)){
                playerSelected.airTime = 0;
                playerSelected.jumpNeed = false;
            }
        }else{

            var rayPlayer = new BABYLON.Ray(playerSelected.playerBox.position,new BABYLON.Vector3(0,-1,0));

           
            var distPlayer = this.game.scene.pickWithRay(rayPlayer, function (item) {
                if (item.name == "hitBoxPlayer" || item.id == "headMainPlayer" || item.id == "bodyGhost"  ||  item.isPlayer || item.id == 'weaponHand')
                    return false;
                else
                    return true;
            });
            
            if(playerSelected.isMain){
                var targetHeight = this.originHeight.y;
            }else{
                var targetHeight = 1;
            }
            if(distPlayer.distance <= targetHeight){
                if(playerSelected.isMain && !playerSelected.canJump){
                    playerSelected.canJump = true;
                }
                playerSelected.airTime = 0;
            }else{
                playerSelected.airTime++;
                playerSelected.playerBox.moveWithCollisions(new BABYLON.Vector3(0,(-playerSelected.airTime/30) * relativeSpeed ,0));
            }
        }
    },
    getDamage : function(damage, whoDamage){
        var damageTaken = damage;

        if(this.camera.armor > Math.round(damageTaken/2)){
            this.camera.armor -= Math.round(damageTaken/2);
            damageTaken = Math.round(damageTaken/2);
        }else{
            damageTaken = damageTaken - this.camera.armor;
            this.camera.armor = 0;
        }

        if(this.camera.health>damageTaken){
            this.camera.health-=damageTaken;
            if(this.camera.isMain){
                this.textHealth.innerText = this.camera.health;
                this.textArmor.innerText = this.camera.armor;
            }
        }else{
            if(this.camera.isMain){
                this.textHealth.innerText = 0;
                this.textArmor.innerText = 0;
            }
            this.playerDead(whoDamage)
        }
    },
    playerDead : function(whoKilled) {
        if(this.displayAnnouncement.classList.contains("annoucementClose")){
            this.displayAnnouncement.classList.remove("annoucementClose");
        }
        this.textDisplayAnnouncement.style.fontSize = '1rem';
        this.textDisplayAnnouncement.innerText = 'Vous êtes mort';

        sendPostMortem(whoKilled);

        this.deadCamera = new BABYLON.ArcRotateCamera("ArcRotateCamera", 
        1, 0.8, 10, new BABYLON.Vector3(
            this.camera.playerBox.position.x, 
            this.camera.playerBox.position.y, 
            this.camera.playerBox.position.z), 
        this.game.scene);
        
        this.game.scene.activeCamera = this.deadCamera;
        this.deadCamera.attachControl(this.game.scene.getEngine().getRenderingCanvas());

        this.camera.playerBox.dispose();

        this.camera.dispose();   

        var inventoryWeapons = this.camera.weapons.inventory;
        for (var i = 0; i < inventoryWeapons.length; i++) {
            inventoryWeapons[i].dispose();
        }
        inventoryWeapons = [];

        this.isAlive=false;

        var newPlayer = this;
        var canvas = this.game.scene.getEngine().getRenderingCanvas();
        setTimeout(function(){ 
            newPlayer._initCamera(newPlayer.game.scene, canvas, newPlayer.spawnPoint);
            newPlayer.displayAnnouncement.classList.add("annoucementClose");
            newPlayer.launchRessurection();
        }, 4000);
    },
    newDeadEnnemy : function(nameKilled){
        var _this = this;

        if(this.killStreak === 0){
            this.textDisplayAnnouncement.style.fontSize = '1rem';
            var messageDisplay = "Vous avez tué Bob"
            if(nameKilled){
                var messageDisplay = "Vous avez tué " + nameKilled;
            }
        }else{
            var multiKillAnouncement = this.camera.weapons.Armory.multiKillAnnoucement;
            if(this.killStreak<=multiKillAnouncement.length){
                var messageDisplay = multiKillAnouncement[this.killStreak-1];
                this.textDisplayAnnouncement.style.fontSize = (1+(this.killStreak/1.2))+'rem';
            }else{
                var messageDisplay = multiKillAnouncement[multiKillAnouncement.length-1]
            }
            
        }
        this.killStreak++;

        if(this.displayAnnouncement.classList.contains("annoucementClose")){
            this.displayAnnouncement.classList.remove("annoucementClose");
        }

        this.textDisplayAnnouncement.innerText = messageDisplay;

        if(this.timerKillStreak){
            clearTimeout(this.timerKillStreak);
        }
		
        // Compteur reset toutes les 5 secs
        this.timerKillStreak = setTimeout(function(){ 
            _this.killStreak = 0;
            
            if(!_this.displayAnnouncement.classList.contains("annoucementClose")){
                _this.displayAnnouncement.classList.add("annoucementClose");

            }
        }, 5000);
    },
	
    givePlayerBonus : function(what,howMany) {
        
        var typeBonus = what;
        var amountBonus = howMany;
        if(typeBonus === 'health'){
            if(this.camera.health + amountBonus>100){
                this.camera.health = 100;
            }else{
                this.camera.health += amountBonus;
            }
        }else if (typeBonus === 'armor'){
            if(this.camera.armor + amountBonus>100){
                this.camera.armor = 100;
            }else{
                this.camera.armor += amountBonus;
            }
        } 
        this.textHealth.innerText = this.camera.health;
        this.textArmor.innerText = this.camera.armor;
    },
	
    // FONCTIONS MULTIJOUEUR
    sendNewData : function(data){
        updateGhost(data);
    },
    launchRessurection : function(){
        ressurectMe();
		this.textHealth.innerText = this.camera.health;
    },
    sendActualData : function(){
        return {
            actualTypeWeapon : this.camera.weapons.actualWeapon,
            armor : this.camera.armor,
            life : this.camera.health,
            position  : this.camera.playerBox.position,
            rotation : this.camera.playerBox.rotation,
            axisMovement : this.camera.axisMovement
        }
    },
    updateLocalGhost : function(data){
        ghostPlayers = this.ghostPlayers;
        
        for (var i = 0; i < ghostPlayers.length; i++) {
            if(ghostPlayers[i].idRoom === data.id){
                var boxModified = ghostPlayers[i].playerBox;
				
                // On applique un correctif sur Y, qui semble tre au mauvais endroit
                if(data.position){
                    boxModified.position = new BABYLON.Vector3(data.position.x,data.position.y-2.76,data.position.z);
                }
                if(data.axisMovement){
                    ghostPlayers[i].axisMovement = data.axisMovement;
                }
                if(data.rotation){
                    ghostPlayers[i].head.rotation.y = data.rotation.y;
                }
                if(data.jumpNeed){
                    ghostPlayers[i].jumpNeed = data.jumpNeed;
                }
                if(data.axisMovement){
                    ghostPlayers[i].axisMovement = data.axisMovement;
                }
            }
            
        }
    }
};