objectColision = [];

Arena = function (game, props) {
    this.game = game;
    this.scene = game.scene;
    this.Armory = game.armory;


    ////////////////// MAP ////////////////////////

    var createSkybox = function (scene) {
        var skyboxMaterial = new BABYLON.SkyMaterial("skyMaterial", scene);
        skyboxMaterial.backFaceCulling = false;
        var skybox = BABYLON.Mesh.CreateBox("skyBox", 4096, scene);
        skybox.material = skyboxMaterial;

        // Reflection probe
        var rp = new BABYLON.ReflectionProbe('ref', 512, scene);
        rp.renderList.push(skybox);
        var pbr = new BABYLON.PBRMaterial('pbr', scene);
        pbr.reflectionTexture = rp.cubeTexture;

        var setSkyConfig = function (property, from, to) {
            var keys = [
                { frame: 0, value: from },
                { frame: 100, value: to }
            ];

            var animation = new BABYLON.Animation("animation", property, 100, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
            animation.setKeys(keys);

            scene.stopAnimation(skybox);
            scene.beginDirectAnimation(skybox, [animation], 0, 100, false, 1);
        };

        let skyTime = 0; // 0 = day | -0.5 = night
        setSkyConfig("material.inclination", skyboxMaterial.inclination, skyTime);

        window.addEventListener("keydown", function (evt) {
            switch (evt.keyCode) {
                case 49:
                    skyTime -= 0.1;
                    setSkyConfig("material.inclination", skyboxMaterial.inclination, skyTime);
                    break; // 1
                case 50:
                    setSkyConfig("material.inclination", skyboxMaterial.inclination, -0.5);
                    break; // 2

                case 51:
                    setSkyConfig("material.luminance", skyboxMaterial.luminance, 0.1);
                    break; // 3
                case 52:
                    setSkyConfig("material.luminance", skyboxMaterial.luminance, 1.0);
                    break; // 4

                case 53:
                    setSkyConfig("material.turbidity", skyboxMaterial.turbidity, 40);
                    break; // 5
                case 54:
                    setSkyConfig("material.turbidity", skyboxMaterial.turbidity, 5);
                    break; // 6
                case 55:
                    setSkyConfig("material.cameraOffset.y", skyboxMaterial.inclination, skyboxMaterial.inclination + 0.1);
                    break; // 7
                case 56:
                    setSkyConfig("material.cameraOffset.y", skyboxMaterial.cameraOffset.y, 0);
                    break;  // 8
                default:
                    break;
            }
        });
        
        return [skybox, pbr];
    };

    let createLight = function (scene) {
        let light = new BABYLON.PointLight("dir01", new BABYLON.Vector3(400, 900, -600), scene);
        let light3 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 10, 0), scene);
        let lightSphere0 = BABYLON.Mesh.CreateSphere("Sphere0", 16, 0.5, scene);

        light.intensity = 1.2;
        light3.intensity = 0.3;

        lightSphere0.material = new BABYLON.StandardMaterial("white", scene);
        lightSphere0.material.diffuseColor = new BABYLON.Color3(0, 0, 0);
        lightSphere0.material.specularColor = new BABYLON.Color3(0, 0, 0);
        lightSphere0.material.emissiveColor = new BABYLON.Color3(1, 1, 1);
        lightSphere0.position = light.position;

        let lensFlareSystem = new BABYLON.LensFlareSystem("lensFlareSystem", light, scene);
        new BABYLON.LensFlare(0.2, 0, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens5.png", lensFlareSystem);
        new BABYLON.LensFlare(0.5, 0.2, new BABYLON.Color3(0.5, 0.5, 1), "texture/soleil/lens4.png", lensFlareSystem);
        new BABYLON.LensFlare(0.2, 1.0, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens4.png", lensFlareSystem);
        new BABYLON.LensFlare(0.4, 0.4, new BABYLON.Color3(1, 0.5, 1), "texture/soleil/Flare.png", lensFlareSystem);
        new BABYLON.LensFlare(0.1, 0.6, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens5.png", lensFlareSystem);
        new BABYLON.LensFlare(0.3, 0.8, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens4.png", lensFlareSystem);

        lightSphere0.isVisible = false;

        return light;
    };

    var createCamera = function (scene) {
        var camera = new BABYLON.FreeCamera("Camera", new BABYLON.Vector3(0, 4, -20), scene);
        camera.speed = 0.5;

        camera.angularSensibility = 1000;
        camera.keysUp = [90]; // Touche Z
        camera.keysDown = [83]; // Touche S
        camera.keysLeft = [81]; // Touche Q
        camera.keysRight = [68]; // Touche D;


        // Collisions
        camera.checkCollisions = true;
        camera.applyGravity = true;

        //camera.ellipsoid = new BABYLON.Vector3(0.1, 1, 0.1);

        return camera;
    };


    var createBorder = function (scene) {
        //délimitation de la map

        var plafond = BABYLON.Mesh.CreateBox("plafond", 1, scene);
        plafond.scaling.z = 240;
        plafond.scaling.x = 80;
        plafond.position.y = 400;
        plafond.position.z = 60;
        plafond.isVisible = false;
        plafond.checkCollisions = true;

        var border = BABYLON.Mesh.CreateBox("border", 350, scene, false, BABYLON.Mesh.BACKSIDE);
        border.checkCollisions = true;
        border.isVisible = false;
        border.position.x = 0;

        return plafond;
    };

    var createWater = function (scene) {

        var waterMaterial = new BABYLON.WaterMaterial("waterMaterial", scene, new BABYLON.Vector2(4096, 4096));
        waterMaterial.bumpTexture = new BABYLON.Texture("texture/water/waterBump.png", scene);
        waterMaterial.windForce = 2;
        waterMaterial.waveHeight = 0.4;
        waterMaterial.bumpHeight = 0.3;
        waterMaterial.waveLength = 0.05;
        waterMaterial.waveSpeed = 0.01;
        waterMaterial.colorBlendFactor = 0.05;
        waterMaterial.windDirection = new BABYLON.Vector2(1, 1);

        // Water mesh
        var waterMesh = BABYLON.Mesh.CreateGround("waterMesh", 4096, 4096, 1, scene, false);
        waterMesh.position.y = 0.03;
        waterMesh.material = waterMaterial;

        return [waterMaterial, waterMesh];
    };

    var mergeMeshes = function (meshName, arrayObj, scene) {
        var arrayPos = [];
        var arrayNormal = [];
        var arrayUv = [];
        var arrayUv2 = [];
        var arrayColor = [];
        var arrayMatricesIndices = [];
        var arrayMatricesWeights = [];
        var arrayIndice = [];
        var savedPosition = [];
        var savedNormal = [];
        var newMesh = new BABYLON.Mesh(meshName, scene);
        var UVKind = true;
        var UV2Kind = true;
        var ColorKind = true;
        var MatricesIndicesKind = true;
        var MatricesWeightsKind = true;

        for (var i = 0; i != arrayObj.length; i++) {
            if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.UVKind]))
                UVKind = false;
            if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.UV2Kind]))
                UV2Kind = false;
            if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.ColorKind]))
                ColorKind = false;
            if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.MatricesIndicesKind]))
                MatricesIndicesKind = false;
            if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.MatricesWeightsKind]))
                MatricesWeightsKind = false;
        }

        for (i = 0; i != arrayObj.length; i++) {
            var ite = 0;
            var iter = 0;
            arrayPos[i] = arrayObj[i].getVerticesData(BABYLON.VertexBuffer.PositionKind);
            arrayNormal[i] = arrayObj[i].getVerticesData(BABYLON.VertexBuffer.NormalKind);
            if (UVKind)
                arrayUv = arrayUv.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.UVKind));
            if (UV2Kind)
                arrayUv2 = arrayUv2.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.UV2Kind));
            if (ColorKind)
                arrayColor = arrayColor.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.ColorKind));
            if (MatricesIndicesKind)
                arrayMatricesIndices = arrayMatricesIndices.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.MatricesIndicesKind));
            if (MatricesWeightsKind)
                arrayMatricesWeights = arrayMatricesWeights.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.MatricesWeightsKind));

            var maxValue = savedPosition.length / 3;

            arrayObj[i].computeWorldMatrix(true);
            var worldMatrix = arrayObj[i].getWorldMatrix();

            for (var ite = 0; ite != arrayPos[i].length; ite += 3) {
                var vertex = new BABYLON.Vector3.TransformCoordinates(new BABYLON.Vector3(arrayPos[i][ite], arrayPos[i][ite + 1], arrayPos[i][ite + 2]), worldMatrix);
                savedPosition.push(vertex.x);
                savedPosition.push(vertex.y);
                savedPosition.push(vertex.z);
            }

            for (var iter = 0; iter != arrayNormal[i].length; iter += 3) {
                var vertex = new BABYLON.Vector3.TransformNormal(new BABYLON.Vector3(arrayNormal[i][iter], arrayNormal[i][iter + 1], arrayNormal[i][iter + 2]), worldMatrix);
                savedNormal.push(vertex.x);
                savedNormal.push(vertex.y);
                savedNormal.push(vertex.z);
            }

            var tmp = arrayObj[i].getIndices();
            for (it = 0; it != tmp.length; it++) {
                arrayIndice.push(tmp[it] + maxValue);
            }
            arrayIndice = arrayIndice.concat(tmp);

            arrayObj[i].dispose(false);
        }

        newMesh.setVerticesData(BABYLON.VertexBuffer.PositionKind, savedPosition, false);
        newMesh.setVerticesData(BABYLON.VertexBuffer.NormalKind, savedNormal, false);
        if (arrayUv.length > 0)
            newMesh.setVerticesData(BABYLON.VertexBuffer.UVKind, arrayUv, false);
        if (arrayUv2.length > 0)
            newMesh.setVerticesData(BABYLON.VertexBuffer.UV2Kind, arrayUv, false);
        if (arrayColor.length > 0)
            newMesh.setVerticesData(BABYLON.VertexBuffer.ColorKind, arrayUv, false);
        if (arrayMatricesIndices.length > 0)
            newMesh.setVerticesData(BABYLON.VertexBuffer.MatricesIndicesKind, arrayUv, false);
        if (arrayMatricesWeights.length > 0)
            newMesh.setVerticesData(BABYLON.VertexBuffer.MatricesWeightsKind, arrayUv, false);

        newMesh.setIndices(arrayIndice);
        return newMesh;
    };

    this.playerBox = this.game._PlayerData.camera.playerBox;
    // Création de notre lumière principale

    // light1 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 10, 0), this.scene);
    // var light2 = new BABYLON.HemisphericLight("light2", new BABYLON.Vector3(0, -1, 0), this.scene);
    // light2.intensity = 0.8;

    this.pipeline = this.createPostProcessPipeline(this.scene);
    this.light = createLight(this.scene);
    this.shadowGenerator = this.createShadowGenerator(this.light);
    let skyBox = createSkybox(this.scene);
    this.skybox = skyBox[0];
    this.skyboxPbrReflectionMaterial = skyBox[1];
    this.border = createBorder(this.scene);
    let water = createWater(this.scene);

    BABYLON.SceneLoader.ImportMesh("", "assets/map/shark/", "shark.glb", this.scene, function (meshes) {
        for(i=0; i<meshes.length;i++) {
            meshes[i].position.y = -45;
            meshes[i].position.z = 900;
            meshes[i].position.x = 174;

            meshes[i].rotation.y = Math.PI/20;
            meshes[i].rotation.x = -Math.PI/14;
            let scaleRatio = 2;
            meshes[i].scaling.addInPlace(new BABYLON.Vector3(3*scaleRatio, 2*scaleRatio, 3*scaleRatio));


        }
    });

    this.waterMaterial = water[0];
    this.waterGround = water[1];
    this.ground = BABYLON.MeshBuilder.CreateGroundFromHeightMap("ground", "../../assets/map/heightmap/forest.png", {
        width: 400, height: 400, subdivisions: 200, maxHeight: 10, minHeight: 0
    });

    // Create terrain material
    var terrainMaterial = new BABYLON.TerrainMaterial("terrainMaterial", this.scene);
    terrainMaterial.specularColor = new BABYLON.Color3(0.5, 0.5, 0.5);
    terrainMaterial.specularPower = 64;

    // Set the mix texture (represents the RGB values)
    terrainMaterial.mixTexture = new BABYLON.Texture("../assets/map/heightmap/mixMap.png", this.scene);

    // Diffuse textures following the RGB values of the mix map
    // diffuseTexture1: Red
    // diffuseTexture2: Green
    // diffuseTexture3: Blue
    terrainMaterial.diffuseTexture1 = new BABYLON.Texture("texture/groundMaterial/grass.jpg", this.scene);
    terrainMaterial.diffuseTexture2 = new BABYLON.Texture("texture/groundMaterial/ground.png", this.scene);
    terrainMaterial.diffuseTexture3 = new BABYLON.Texture("texture/groundMaterial/sand.png", this.scene);

    // Bump textures according to the previously set diffuse textures
    terrainMaterial.bumpTexture1 = new BABYLON.Texture("texture/groundMaterial/bump/grassn.png", this.scene);
    terrainMaterial.bumpTexture2 = new BABYLON.Texture("texture/groundMaterial/bump/rockn.png", this.scene);
    terrainMaterial.bumpTexture3 = new BABYLON.Texture("texture/groundMaterial/bump/grassn.png", this.scene);

    // Rescale textures according to the terrain
    terrainMaterial.diffuseTexture1.uScale = terrainMaterial.diffuseTexture1.vScale = 50;
    terrainMaterial.diffuseTexture2.uScale = terrainMaterial.diffuseTexture2.vScale = -30;
    terrainMaterial.diffuseTexture3.uScale = terrainMaterial.diffuseTexture3.vScale = 10;

    this.ground.material = terrainMaterial;
    this.ground.receiveShadows = true;
    this.ground.checkCollisions = true;

    this.ground.onReady = () => {
        this.createTrees(this.ground, 300, 50, (trees) => {
            for (let x = 0; x < trees.length; x++) {
                trees[x].specularColor = new BABYLON.Color3(5, 5, 5);
                trees[x].emissiveColor = new BABYLON.Color3(0, 0, 0);
                this.shadowGenerator.getShadowMap().renderList.push(trees[x]);
                this.waterMaterial.addToRenderList(trees[x]);
            }
        });

        this.createBushs(this.ground, 200, 200, (bushs) => {
            for (let x = 0; x < bushs.length; x++) {
                // bushs[x].specularColor = new BABYLON.Color3(5, 5, 5);
                // bushs[x].emissiveColor = new BABYLON.Color3(0, 0, 0);
                // this.shadowGenerator.getShadowMap().renderList.push(bushs[x]);
                // this.waterMaterial.addToRenderList(bushs[x]);
            }
        });

        this.createCabane(this.scene, this.ground, (cabane) => {
            this.shadowGenerator.getShadowMap().renderList.push(cabane);
            this.waterMaterial.addToRenderList(cabane);
        })
    };


    //reflection & refraction
    this.waterMaterial.addToRenderList(this.skybox);
    this.waterMaterial.addToRenderList(this.playerBox);
    PLAYER = this.playerBox;
    this.waterMaterial.addToRenderList(this.ground);
    // shadow
    this.shadowGenerator.getShadowMap().renderList.push(this.playerBox);
    this.shadowGenerator.getShadowMap().refreshRate = 0; // We need to compute it just once
    // this.scene.createOrUpdateSelectionOctree();
    // DEFINITION DES PROPS ------------------------------------------------

    // Liste des objets stocké dans le jeu
    this.bonusBox = [];
    this.weaponBox = [];
    this.ammosBox = [];

    // Les props envoyé par le serveur
    this.bonusServer = props[0];
    this.weaponServer = props[1];
    this.ammosServer = props[2];

    for (var i = 0; i < this.bonusServer.length; i++) {
        // Si l'objet n'a pas été pris par un joueur
        if (this.bonusServer[i].v === 1) {
            var newBonusBox = this.newBonuses(new BABYLON.Vector3(
                    this.bonusServer[i].x,
                    this.bonusServer[i].y,
                    this.bonusServer[i].z),
                this.bonusServer[i].t);

            newBonusBox.idServer = i;
            this.bonusBox.push(newBonusBox);
        }
    }

    for (var i = 0; i < this.weaponServer.length; i++) {
        if (this.weaponServer[i].v === 1) {
            var newWeaponBox = this.newWeaponSet(new BABYLON.Vector3(
                    this.weaponServer[i].x,
                    this.weaponServer[i].y,
                    this.weaponServer[i].z),
                this.weaponServer[i].t);

            newWeaponBox.idServer = i;
            this.weaponBox.push(newWeaponBox);
        }
    }

    for (var i = 0; i < this.ammosServer.length; i++) {
        if (this.ammosServer[i].v === 1) {
            var newAmmoBox = this.newAmmo(new BABYLON.Vector3(
                    this.ammosServer[i].x,
                    this.ammosServer[i].y,
                    this.ammosServer[i].z),
                this.ammosServer[i].t);

            newAmmoBox.idServer = i;
            this.ammosBox.push(newAmmoBox);
        }
    }
};

Arena.prototype = {
    createPostProcessPipeline: function (scene) {

        // scene.fogMode = BABYLON.Scene.FOGMODE_EXP;
        // scene.fogColor = new BABYLON.Color3(0.9, 0.9, 0.85);
        // scene.fogDensity = 0.0010;

        // var defaultPipeline = new BABYLON.DefaultRenderingPipeline(
        //     "DefaultRenderingPipeline",
        //     true, // is HDR?
        //     scene,
        //     scene.cameras
        // );
        // if (defaultPipeline.isSupported) {
        //     /* MSAA */
        //     defaultPipeline.samples = 1; // 1 by default
        //     /* imageProcessing */
        //     defaultPipeline.imageProcessingEnabled = true; //true by default
        //     if (defaultPipeline.imageProcessingEnabled) {
        //         defaultPipeline.imageProcessing.contrast = 1; // 1 by default
        //         defaultPipeline.imageProcessing.exposure = 1; // 1 by default
        //         /* color grading */
        //         defaultPipeline.imageProcessing.colorGradingEnabled = false; // false by default
        //         if (defaultPipeline.imageProcessing.colorGradingEnabled) {
        //             // using .3dl (best) :
        //             defaultPipeline.imageProcessing.colorGradingTexture = new BABYLON.ColorGradingTexture("textures/LateSunset.3dl", scene);
        //             // using .png :
        //             /*
        //             var colorGradingTexture = new BABYLON.Texture("textures/colorGrade-highContrast.png", scene, true, false);
        //             colorGradingTexture.wrapU = BABYLON.Texture.CLAMP_ADDRESSMODE;
        //             colorGradingTexture.wrapV = BABYLON.Texture.CLAMP_ADDRESSMODE;
        //             defaultPipeline.imageProcessing.colorGradingTexture = colorGradingTexture;
        //             defaultPipeline.imageProcessing.colorGradingWithGreenDepth = false;
        //             */
        //         }
        //         /* color curves */
        //         defaultPipeline.imageProcessing.colorCurvesEnabled = false; // false by default
        //         if (defaultPipeline.imageProcessing.colorCurvesEnabled) {
        //             var curve = new BABYLON.ColorCurves();
        //             curve.globalDensity = 0; // 0 by default
        //             curve.globalExposure = 0; // 0 by default
        //             curve.globalHue = 30; // 30 by default
        //             curve.globalSaturation = 0; // 0 by default
        //             curve.highlightsDensity = 0; // 0 by default
        //             curve.highlightsExposure = 0; // 0 by default
        //             curve.highlightsHue = 30; // 30 by default
        //             curve.highlightsSaturation = 0; // 0 by default
        //             curve.midtonesDensity = 0; // 0 by default
        //             curve.midtonesExposure = 0; // 0 by default
        //             curve.midtonesHue = 30; // 30 by default
        //             curve.midtonesSaturation = 0; // 0 by default
        //             curve.shadowsDensity = 0; // 0 by default
        //             curve.shadowsExposure = 0; // 0 by default
        //             curve.shadowsHue = 30; // 30 by default
        //             curve.shadowsDensity = 80;
        //             curve.shadowsSaturation = 0; // 0 by default;
        //             defaultPipeline.imageProcessing.colorCurves = curve;
        //         }
        //     }
        //     /* bloom */
        //     defaultPipeline.bloomEnabled = true; // false by default
        //     if (defaultPipeline.bloomEnabled) {
        //         defaultPipeline.bloomKernel = 64; // 64 by default
        //         defaultPipeline.bloomScale = 0.5; // 0.5 by default
        //         defaultPipeline.bloomThreshold = 0.9; // 0.9 by default
        //         defaultPipeline.bloomWeight = 0.15; // 0.15 by default
        //     }
        //     /* chromatic abberation */
        //     defaultPipeline.chromaticAberrationEnabled = false; // false by default
        //     if (defaultPipeline.chromaticAberrationEnabled) {
        //         defaultPipeline.chromaticAberration.aberrationAmount = 30; // 30 by default
        //         defaultPipeline.chromaticAberration.adaptScaleToCurrentViewport = false; // false by default
        //         defaultPipeline.chromaticAberration.alphaMode = 0; // 0 by default
        //         defaultPipeline.chromaticAberration.alwaysForcePOT = false; // false by default
        //         defaultPipeline.chromaticAberration.enablePixelPerfectMode = false; // false by default
        //         defaultPipeline.chromaticAberration.forceFullscreenViewport = true; // true by default
        //     }
        //     /* DOF */
        //     defaultPipeline.depthOfFieldEnabled = false; // false by default
        //     if (defaultPipeline.depthOfFieldEnabled && defaultPipeline.depthOfField.isSupported) {
        //         defaultPipeline.depthOfFieldBlurLevel = 0; // 0 by default
        //         defaultPipeline.depthOfField.fStop = 1.4; // 1.4 by default
        //         defaultPipeline.depthOfField.focalLength = 50; // 50 by default, mm
        //         defaultPipeline.depthOfField.focusDistance = 2000; // 2000 by default, mm
        //         defaultPipeline.depthOfField.lensSize = 50; // 50 by default
        //     }
        //     /* FXAA */
        //     defaultPipeline.fxaaEnabled = true; // false by default
        //     if (defaultPipeline.fxaaEnabled) {
        //         defaultPipeline.fxaa.samples = 1; // 1 by default
        //         defaultPipeline.fxaa.adaptScaleToCurrentViewport = false; // false by default
        //     }
        //     /* glowLayer */
        //     defaultPipeline.glowLayerEnabled = false;
        //     if (defaultPipeline.glowLayerEnabled) {
        //         defaultPipeline.glowLayer.blurKernelSize = 16; // 16 by default
        //         defaultPipeline.glowLayer.intensity = 1; // 1 by default
        //     }
        //     /* grain */
        //     defaultPipeline.grainEnabled = false;
        //     if (defaultPipeline.grainEnabled) {
        //         defaultPipeline.grain.adaptScaleToCurrentViewport = false; // false by default
        //         defaultPipeline.grain.animated = false; // false by default
        //         defaultPipeline.grain.intensity = 30; // 30 by default
        //     }
        //     /* sharpen */
        //     defaultPipeline.sharpenEnabled = false;
        //     if (defaultPipeline.sharpenEnabled) {
        //         defaultPipeline.sharpen.adaptScaleToCurrentViewport = false; // false by default
        //         defaultPipeline.sharpen.edgeAmount = 0.3; // 0.3 by default
        //         defaultPipeline.sharpen.colorAmount = 1; // 1 by default
        //     }
        // }

        var defaultPipeline = new BABYLON.DefaultRenderingPipeline("default", true, scene, scene.cameras);
        var curve = new BABYLON.ColorCurves();
        curve.globalHue = 200;
        curve.globalDensity = 80;
        curve.globalSaturation = 80;
        curve.highlightsHue = 20;
        curve.highlightsDensity = 80;
        curve.highlightsSaturation = -80;
        curve.shadowsHue = 2;
        curve.shadowsDensity = 80;
        curve.shadowsSaturation = 40;
        defaultPipeline.imageProcessing.colorCurves = curve;
        defaultPipeline.depthOfField.focalLength = 150;


        return defaultPipeline;
    },
    createShadowGenerator: function (light) {
        let shadowGenerator = new BABYLON.ShadowGenerator(2048, light);
        shadowGenerator.useBlurExponentialShadowMap = true;
        shadowGenerator.blurBoxOffset = 4;
        shadowGenerator.enableSoftTransparentShadow = false;
        shadowGenerator.transparencyShadow = true;

        return shadowGenerator;
    },
    terrainGenListener: function (scene, elevationControl, waterMaterial) {
// UI
        var camera = scene.activeCamera;
        var cameraButton = document.getElementById("cameraButton");
        var elevationButton = document.getElementById("elevationButton");
        var digButton = document.getElementById("digButton");
        var saveButton = document.getElementById("saveButton");
        var canvas = document.getElementById("renderCanvas");
        var mode = "CAMERA";

        saveButton.addEventListener("click", function () {
            save(scene.getMeshByName("ground"));
        });

        cameraButton.addEventListener("pointerdown", function () {

            if (mode == "CAMERA")
                return;
            camera.attachControl(canvas);
            elevationControl.detachControl(canvas);

            mode = "CAMERA";
            cameraButton.className = "controlButton selected";
            digButton.className = "controlButton";
            elevationButton.className = "controlButton";
        });

        elevationButton.addEventListener("pointerdown", function () {

            if (mode == "ELEVATION")
                return;

            if (mode == "CAMERA") {
                camera.detachControl(canvas);
                elevationControl.attachControl(canvas);
            }

            mode = "ELEVATION";
            elevationControl.direction = 1;

            elevationButton.className = "controlButton selected";
            digButton.className = "controlButton";
            cameraButton.className = "controlButton";
        });

        digButton.addEventListener("pointerdown", function () {

            if (mode == "DIG")
                return;

            if (mode == "CAMERA") {
                camera.detachControl(canvas);
                elevationControl.attachControl(canvas);
            }

            mode = "DIG";
            elevationControl.direction = -1;

            digButton.className = "controlButton selected";
            elevationButton.className = "controlButton";
            cameraButton.className = "controlButton";
        });

        // Sliders
        $("#slider-vertical").slider({
            orientation: "vertical",
            range: "min",
            min: 2,
            max: 15,
            value: 5,
            slide: function (event, ui) {
                elevationControl.radius = ui.value;
            }
        });

        $("#slider-range").slider({
            orientation: "vertical",
            range: true,
            min: 0,
            max: 12,
            values: [0, 11],
            slide: function (event, ui) {
                elevationControl.heightMin = ui.values[0];
                elevationControl.heightMax = ui.values[1];
            }
        });

        $("#qualitySlider").slider({
            orientation: "vertical",
            range: "min",
            min: 0,
            max: 4,
            value: 3,
            slide: function (event, ui) {
                switch (ui.value) {
                    case 4:
                        waterMaterial.refractionTexture.resize(512, true);
                        waterMaterial.reflectionTexture.resize(512, true);
                        scene.getEngine().setHardwareScalingLevel(1);
                        scene.particlesEnabled = true;
                        scene.postProcessesEnabled = true;
                        break;
                    case 3:
                        waterMaterial.refractionTexture.resize(256, true);
                        waterMaterial.reflectionTexture.resize(256, true);
                        scene.getEngine().setHardwareScalingLevel(1);
                        scene.particlesEnabled = false;
                        scene.postProcessesEnabled = false;
                        break;
                    case 2:
                        waterMaterial.refractionTexture.resize(256, true);
                        waterMaterial.reflectionTexture.resize(256, true);
                        scene.getEngine().setHardwareScalingLevel(2);
                        scene.particlesEnabled = false;
                        scene.postProcessesEnabled = false;
                        break;
                    case 1:
                        waterMaterial.refractionTexture.resize(256, true);
                        waterMaterial.reflectionTexture.resize(256, true);
                        scene.getEngine().setHardwareScalingLevel(3);
                        scene.particlesEnabled = false;
                        scene.postProcessesEnabled = false;
                        break;
                    case 0:
                        waterMaterial.refractionTexture.resize(256, true);
                        waterMaterial.reflectionTexture.resize(256, true);
                        scene.getEngine().setHardwareScalingLevel(4);
                        scene.particlesEnabled = false;
                        scene.postProcessesEnabled = false;
                        break;
                }
            }
        });
    },
    createEnemi: function (scene, ground) {
        var camera = scene.activeCamera;

        // Dude
        BABYLON.SceneLoader.ImportMesh("him", "assets/map/homme/", "Dude.babylon", scene, function (newMeshes2, particleSystems2, skeletons2) {
            dude = newMeshes2[0];

            dude.scaling = new BABYLON.Vector3(0.04, 0.03, 0.04);
            dude.rotation.y = Math.PI;
            dude.position = new BABYLON.Vector3(5, ground.getHeightAtCoordinates(5, 5), 5);
            dude.checkCollisions = true;

            //dude.setPhysicsState({ impostor: BABYLON.PhysicsEngine.BoxImpostor, move:true});

            scene.beginAnimation(skeletons2[0], 0, 100, true, 1.0);

            //Bot recherche
            scene.registerBeforeRender(function () {
                Arena.prototype.bot(dude, camera, ground);
            });

            return dude;
        });

    },
    creerMap: function (map, name) {

        if (this.game._PlayerData.name === name) {

            var ground = BABYLON.Mesh.CreateGround("ground", 200, 200, 150, this.scene, true);
            //ground.setPhysicsState({ impostor: BABYLON.PhysicsEngine.BoxImpostor, move:false});
            ground.receiveShadows = true;
            ground.checkCollisions = true;

            var groundMaterial = new GROUNDMATERIAL.GroundMaterial("groundMaterial", this.scene, this.light);
            groundMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
            groundMaterial.emissiveColor = new BABYLON.Color3(0.3, 0.3, 0.3);

            ground.material = groundMaterial;

            var buffer = new WORLDMONGER.Ground(ground, map[0], map[1]);

            buffer.Load();

            if (ground.isReady() === true) {

                var rayo = new BABYLON.Ray(new BABYLON.Vector3(this.playerBox.position.x, 20, this.playerBox.position.z), new BABYLON.Vector3(0, -1, 0));

                var meshFound = this.scene.pickWithRay(rayo, function (item) {
                    if (item === ground)
                        return true;
                });

                if (meshFound.pickedPoint !== null) {
                    this.playerBox.position.y = meshFound.pickedPoint.y + 7;
                    // alert(meshFound.pickedPoint.y);
                }

            }

            this.waterMaterial.addToRenderList(ground);


            // ground.optimize(0);
            // ground.isPickable = false;

        }

    },
    newBonuses: function (position, type) {
        var typeBonus = type;
        var positionBonus = position;

        // On crée un cube
        var newBonus = BABYLON.Mesh.CreateBox("bonusItem", 2, this.game.scene);
        newBonus.scaling = new BABYLON.Vector3(1, 1, 1);

        // On lui donne la couleur orange
        newBonus.material = new BABYLON.StandardMaterial("textureItem", this.game.scene);
        newBonus.material.diffuseColor = new BABYLON.Color3((255 / 255), (138 / 255), (51 / 255));

        // On positionne l'objet selon la position envoyé
        newBonus.position = positionBonus;

        // On le rend impossible a être séléctionné par les raycast
        newBonus.isPickable = false;

        // On affecte à l'objet son type
        newBonus.typeBonus = typeBonus;

        return newBonus;
    },
    newWeaponSet: function (position, type) {
        var typeWeapons = type;
        var positionWeapon = position;

        var newSetWeapon = BABYLON.Mesh.CreateBox(this.Armory.weapons[typeWeapons].name, 1, this.game.scene);
        newSetWeapon.scaling = new BABYLON.Vector3(1, 0.7, 2);


        newSetWeapon.material = new BABYLON.StandardMaterial("weaponMat", this.game.scene);
        newSetWeapon.material.diffuseColor = this.Armory.weapons[typeWeapons].setup.colorMesh;
        newSetWeapon.position = positionWeapon;
        newSetWeapon.isPickable = false;
        newSetWeapon.typeWeapon = type;

        return newSetWeapon;
    },
    newAmmo: function (position, type) {
        var typeAmmos = type;
        var positionAmmo = position;
        var newAmmo = BABYLON.Mesh.CreateBox(this.game.armory.weapons[typeAmmos].name, 1.0, this.game.scene);
        newAmmo.position = positionAmmo;
        newAmmo.isPickable = false;
        newAmmo.material = new BABYLON.StandardMaterial("ammoMat", this.game.scene);
        newAmmo.material.diffuseColor = this.game.armory.weapons[typeAmmos].setup.colorMesh;
        newAmmo.typeAmmo = type;

        return newAmmo;
    },
    _checkProps: function () {
        // Pour les bonus
        for (var i = 0; i < this.bonusBox.length; i++) {
            // On vérifie si la distance est inférieure à 6
            if (BABYLON.Vector3.Distance(
                this.playerBox.position,
                this.bonusBox[i].position) < 6) {
                var paramsBonus = this.Armory.bonuses[this.bonusBox[i].typeBonus];

                this.game._PlayerData.givePlayerBonus(paramsBonus.type, paramsBonus.value);

                // Pour la boucle bonusBox
                this.displayNewPicks(paramsBonus.message);

                // Pour bonusBox
                this.pickableDestroyed(this.bonusBox[i].idServer, 'bonus');

                // On supprime l'objet
                this.bonusBox[i].dispose();
                this.bonusBox.splice(i, 1)
            }

        }
        for (var i = 0; i < this.weaponBox.length; i++) {
            // Pour les armes
            if (BABYLON.Vector3.Distance(
                this.playerBox.position,
                this.weaponBox[i].position) < 6) {

                var Weapons = this.game._PlayerData.camera.weapons;
                var paramsWeapon = this.Armory.weapons[this.weaponBox[i].typeWeapon];
                var notPiked = true;
                for (var y = 0; y < Weapons.inventory.length; y++) {
                    if (Weapons.inventory[y].typeWeapon == this.weaponBox[i].typeWeapon) {
                        notPiked = false;
                        break;
                    }
                }
                if (notPiked) {

                    var actualInventoryWeapon = Weapons.inventory[Weapons.actualWeapon];

                    var newWeapon = Weapons.newWeapon(paramsWeapon.name);
                    Weapons.inventory.push(newWeapon);

                    // On réinitialise la position de l'arme précédente animé
                    actualInventoryWeapon.position = actualInventoryWeapon.basePosition.clone();
                    actualInventoryWeapon.rotation = actualInventoryWeapon.baseRotation.clone();
                    Weapons._animationDelta = 0;

                    actualInventoryWeapon.isActive = false;

                    Weapons.actualWeapon = Weapons.inventory.length - 1;
                    actualInventoryWeapon = Weapons.inventory[Weapons.actualWeapon];

                    actualInventoryWeapon.isActive = true;

                    Weapons.fireRate = Weapons.Armory.weapons[actualInventoryWeapon.typeWeapon].setup.cadency;
                    Weapons._deltaFireRate = Weapons.fireRate;

                    Weapons.textAmmos.innerText = actualInventoryWeapon.ammos;

                    Weapons.totalTextAmmos.innerText =
                        Weapons.Armory.weapons[actualInventoryWeapon.typeWeapon].setup.ammos.maximum;

                    Weapons.typeTextWeapon.innerText =
                        Weapons.Armory.weapons[actualInventoryWeapon.typeWeapon].name;

                    // Pour la boucle weaponBox
                    this.displayNewPicks(paramsWeapon.name);

                    // Pour weaponBox
                    this.pickableDestroyed(this.weaponBox[i].idServer, 'weapon');

                    this.weaponBox[i].dispose();
                    this.weaponBox.splice(i, 1);
                }
            }
        }
        for (var i = 0; i < this.ammosBox.length; i++) {
            // Pour les munitions
            if (BABYLON.Vector3.Distance(
                this.playerBox.position,
                this.ammosBox[i].position) < 6) {

                var paramsAmmos = this.Armory.weapons[this.ammosBox[i].typeAmmo].setup.ammos;
                var Weapons = this.game._PlayerData.camera.weapons;

                Weapons.reloadWeapon(this.ammosBox[i].typeAmmo, paramsAmmos.refuel);

                // Pour la boucle ammosBox
                this.displayNewPicks(paramsAmmos.meshAmmosName);

                // Pour ammosBox
                this.pickableDestroyed(this.ammosBox[i].idServer, 'ammos');

                this.ammosBox[i].dispose();
                this.ammosBox.splice(i, 1)
            }

        }
    },
    // Donner un bonus au joueur
    givePlayerBonus: function (what, howMany) {

        var typeBonus = what;
        var amountBonus = howMany;
        if (typeBonus === 'health') {
            if (this.camera.health + amountBonus > 100) {
                this.camera.health = 100;
            } else {
                this.camera.health += amountBonus;
            }
        } else if (typeBonus === 'armor') {
            if (this.camera.armor + amountBonus > 100) {
                this.camera.armor = 100;
            } else {
                this.camera.armor += amountBonus;
            }
        }
        this.textHealth.innerText = this.camera.health;
        this.textArmor.innerText = this.camera.armor;
    },
    deletePropFromServer: function (deletedProp) {
        // idServer est l'id de l'arme
        var idServer = deletedProp[0];

        // type nous permet de déterminer ce qu'est l'objet
        var type = deletedProp[1];
        switch (type) {
            case 'ammos' :
                for (var i = 0; i < this.ammosBox.length; i++) {
                    if (this.ammosBox[i].idServer === idServer) {
                        this.ammosBox[i].dispose();
                        this.ammosBox.splice(i, 1);
                        break;
                    }
                }

                break;
            case 'bonus' :
                for (var i = 0; i < this.bonusBox.length; i++) {
                    if (this.bonusBox[i].idServer === idServer) {
                        this.bonusBox[i].dispose();
                        this.bonusBox.splice(i, 1);
                        break;
                    }
                }
                break;
            case 'weapon' :
                for (var i = 0; i < this.weaponBox.length; i++) {
                    if (this.weaponBox[i].idServer === idServer) {
                        this.weaponBox[i].dispose();
                        this.weaponBox.splice(i, 1);
                        break;
                    }
                }
                break;
        }
    },
    recreatePropFromServer: function (recreatedProp) {
        var idServer = recreatedProp[0];
        var type = recreatedProp[1];
        switch (type) {
            case 'ammos' :
                var newAmmoBox = this.newAmmo(new BABYLON.Vector3(
                        this.ammosServer[idServer].x,
                        this.ammosServer[idServer].y,
                        this.ammosServer[idServer].z),
                    this.ammosServer[idServer].t);

                newAmmoBox.idServer = idServer;
                this.ammosBox.push(newAmmoBox);
                break;
            case 'bonus' :
                var newBonusBox = this.newBonuses(new BABYLON.Vector3(
                        this.bonusServer[idServer].x,
                        this.bonusServer[idServer].y,
                        this.bonusServer[idServer].z),
                    this.bonusServer[idServer].t);

                newBonusBox.idServer = idServer;
                this.bonusBox.push(newBonusBox);
                break;
            case 'weapon' :
                var newWeaponBox = this.newWeaponSet(new BABYLON.Vector3(
                        this.weaponServer[idServer].x,
                        this.weaponServer[idServer].y,
                        this.weaponServer[idServer].z),
                    this.weaponServer[idServer].t);

                newWeaponBox.idServer = idServer;
                this.weaponBox.push(newWeaponBox);
                break;
        }
    },
    // Partie Server
    pickableDestroyed: function (idServer, type) {
        destroyPropsToServer(idServer, type)
    },
    displayNewPicks: function (typeBonus) {
        // Récupère les propriétés de la fênetre d'annonce
        var displayAnnouncement = document.getElementById('announcementKill');
        var textDisplayAnnouncement = document.getElementById('textAnouncement');

        // Si la fenêtre possède announcementClose (et qu'elle est donc fermé)
        if (displayAnnouncement.classList.contains("annoucementClose")) {
            displayAnnouncement.classList.remove("annoucementClose");
        }
        // On vérifie que la police est à 1 (nous verrons plus tard pourquoi)
        textDisplayAnnouncement.style.fontSize = '1rem';

        // On donne a textDisplayAnnouncement la valeur envoyé à displayNewPicks
        textDisplayAnnouncement.innerText = typeBonus;

        // Au bout de 4 secondes, si la fenêtre est ouverte, on la fait disparaitre
        setTimeout(function () {
            if (!displayAnnouncement.classList.contains("annoucementClose")) {
                displayAnnouncement.classList.add("annoucementClose");
            }
        }, 4000);
    },
    createFullMap: function (scene, light, ground) {

        var light3 = new BABYLON.PointLight("Spot0", new BABYLON.Vector3(-40, 10, -100), scene);
        light3.intensity = 0.3;
        light3.specular = new BABYLON.Color3(0, 0, 0);

        var waterMaterial = scene.getMaterialByName("waterMaterial");
        var camera = scene.activeCamera;

        // waterMaterial.addToRenderList(ground);

        // Creation de l'elevation des sommets
        var elevationControl = new WORLDMONGER.ElevationControl(ground);

        // Shadows
        var shadowGenerator = new BABYLON.ShadowGenerator(2048, light3);
        shadowGenerator.usePoissonSampling = true;
        shadowGenerator.bias = 0.0005;

        this.createEnemi(scene, ground);

        //nom / path / filename / position / rotation / scaling / shadowGenerator / callback

        this.terrainGenListener(scene, elevationControl, waterMaterial);
        console.log("terrain chargé");
    },
    importMesh: function (scene, name, path, file, position, rotation, scaling, callback) {

        BABYLON.SceneLoader.ImportMesh(name, path, file, scene, function (newMeshes, particleSystems, skeletons) {
            newMeshes[0].rotationQuaternion = null; // permet d'utiliser rotation (vecteur 3) a la place de rotationQuaternion (vecteur 4)
            newMeshes[0].position = new BABYLON.Vector3(parseFloat(position.x), newMeshes[0].position.y + parseFloat(position.y), parseFloat(position.z));
            newMeshes[0].rotation = new BABYLON.Vector3(parseFloat(rotation.x), parseFloat(rotation.y), parseFloat(rotation.z));
            newMeshes[0].scaling = new BABYLON.Vector3(parseFloat(scaling.x), parseFloat(scaling.y), parseFloat(scaling.z));

            for (let i = 0; i < newMeshes.length; i++) {
                newMeshes[i].checkCollisions = true;
                newMeshes[i].receiveShadows = true;
                console.log(newMeshes[i]);
            }
            callback(newMeshes[0]);
        });

    },
    createCabane: function (scene, ground, callback) {
        let position = {x: -5, y: null, z: 20 };
        this.importMesh(scene, "cabane", "assets/map/cabane/", "cabane.babylon", {
            x: position.x,
            y: ground.getHeightAtCoordinates(position.x, position.z)+0.5,
            z: position.z
        }, {
            x: 0,
            y: -55,
            z: 0
        }, {x: 1.3, y: 1, z: 1}, (mesh) => {

            var porte = scene.getMeshByName("porte");
            porte.actionManager = new BABYLON.ActionManager(scene);

            var ConditionOpenDoor = new BABYLON.ValueCondition(porte.actionManager, porte, "visibility", 1, BABYLON.ValueCondition.IsEqual);
            porte.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
                    function () {
                        porte.dispose();
                    }, ConditionOpenDoor)
            );

            callback(mesh);
        });

    },
    createTrees: function (ground, range, countTree, callback) {
        let treesBase = [{"x":-76.25324129735839,"y":-0.02524448849549671,"z":-70.89373445663517,"scale":94.01736512603904},{"x":-82.84536477780887,"y":5.1660472439379355,"z":30.234358434643326,"scale":113.45730377079346},{"x":109.56064966635225,"y":2.428704694673523,"z":36.42405419117895,"scale":76.55787417860394},{"x":-120.08902258492657,"y":-0.5,"z":-96.97253879579492,"scale":66.09750831936587},{"x":55.505083923811554,"y":-0.24093350685660347,"z":143.37872617970868,"scale":30.18547536503011},{"x":104.92983827978108,"y":-0.5,"z":-9.959672852145957,"scale":57.96678562400144},{"x":-88.44009026153083,"y":3.4262546534364415,"z":46.01991963742047,"scale":86.24055589592288},{"x":46.28172498845791,"y":7.510110743447916,"z":81.47774959240242,"scale":65.16366816128551},{"x":111.73386430486298,"y":2.87046869949402,"z":-72.91445624652141,"scale":116.41946475411586},{"x":32.41480624911705,"y":5.64049347991429,"z":78.58258373781437,"scale":122.6343506885934},{"x":-12.497351344873863,"y":0.009803921568627416,"z":-74.82768769940861,"scale":56.193777412603275},{"x":-22.083542142561498,"y":4.598039215686273,"z":-6.3998703554082965,"scale":55.385003929962195},{"x":6.589574866792248,"y":4.180528521388102,"z":87.09665508720792,"scale":50.08583393206343},{"x":-8.927820936944357,"y":3.6568627450980387,"z":81.85601269351379,"scale":75.87567239556148},{"x":-50.6184038598455,"y":-0.5,"z":-103.12024340242326,"scale":42.95939342627457},{"x":-39.33396648528023,"y":-0.5,"z":-137.31066245819648,"scale":66.16732536517523},{"x":-138.74078613384432,"y":-0.5,"z":-22.289373168205543,"scale":54.37809901415764},{"x":-89.45992503991374,"y":2.292354685817226,"z":71.22599719668155,"scale":41.62310303418806},{"x":62.95244776887033,"y":0.009803921568627416,"z":-68.21500737944564,"scale":75.67461076824534},{"x":-117.00794091590376,"y":0.06816033828017043,"z":-31.64316323367504,"scale":32.98095440618407},{"x":-21.80589259854645,"y":8.049013040989628,"z":62.1240092389709,"scale":50.6102179062965},{"x":76.98698973668603,"y":3.224463679739594,"z":60.96091733005393,"scale":56.857511622895714},{"x":-62.68001416720449,"y":2.281173137128355,"z":130.7578698825802,"scale":54.270298508054005},{"x":-35.688045461885054,"y":3.6568627450980387,"z":107.07414370503324,"scale":47.32106925985564},{"x":-12.693703657562168,"y":0.1989189465175044,"z":-68.37480203116803,"scale":88.94183002121599},{"x":136.5627487831266,"y":-0.5,"z":18.44823911911385,"scale":31.90271609578359},{"x":-94.56454826700048,"y":-0.5,"z":-133.9998793687775,"scale":112.01622723629796},{"x":-60.86870841637983,"y":6.189786340939429,"z":54.67303147328491,"scale":93.04512402666813},{"x":-57.61400495506831,"y":3.787961322577993,"z":110.00796893152207,"scale":123.42825509396438},{"x":-29.572275313535783,"y":3.9721374402379146,"z":89.39322287506451,"scale":71.81117801438302},{"x":-43.93600293057719,"y":7.628648292822062,"z":62.97545681072867,"scale":120.90844279420054},{"x":83.02376604378435,"y":-0.5,"z":142.6417355506315,"scale":27.892332341734495},{"x":-88.11316360498108,"y":0.08838386810918353,"z":-59.45661648893605,"scale":40.85823620553266},{"x":-102.59478543600488,"y":-0.5,"z":-131.7915580308794,"scale":39.80224953839924},{"x":47.02398442229918,"y":1.068627450980392,"z":-14.023303119540088,"scale":106.95390604340646},{"x":-110.89256086549744,"y":0.009803921568627416,"z":-58.12274501033622,"scale":62.765286808754595},{"x":-124.22463371984048,"y":-0.5,"z":-140.51719269735895,"scale":73.15545644390377},{"x":93.58979178957338,"y":1.1538414430420771,"z":17.741016150687898,"scale":90.7449127810558},{"x":34.510586264256276,"y":6.846491910078403,"z":119.95347169564148,"scale":26.713108219483715},{"x":2.9389665129319553,"y":-0.5,"z":-103.23907002290815,"scale":56.30022416342722},{"x":-133.8849017746042,"y":-0.5,"z":-107.02627207526712,"scale":53.43630594074102},{"x":-96.11095937190447,"y":2.4949668211050002,"z":-27.034773379836025,"scale":27.72316470306788},{"x":12.365287704141167,"y":0.009803921568627416,"z":-89.91107035418091,"scale":28.35512695393072},{"x":56.156625863966966,"y":2.6756569014658838,"z":4.413587685134701,"scale":108.90442263703903},{"x":-147.80272071086256,"y":-0.5,"z":66.10724563788139,"scale":72.45644700743466},{"x":-35.72261052015958,"y":-0.5,"z":-103.91447241419712,"scale":68.08798858282898},{"x":48.4390504061619,"y":0.009803921568627416,"z":-39.53821417153367,"scale":60.84950008611323},{"x":-85.7568875904264,"y":0.020177793500451346,"z":119.85702247052528,"scale":29.896865744107927},{"x":102.53820668669823,"y":0.014609254010851891,"z":16.081690651517818,"scale":65.71913166670328},{"x":-24.749911785071788,"y":6.805394141731314,"z":72.10955331171363,"scale":37.77516310607545}];
        let trees = [];
        let treejson = [];

        BABYLON.SceneLoader.ImportMesh("", "assets/map/arbre/", "tree.babylon", this.scene, function (baseTree) {
            baseTree[0].material.opacityTexture = null;
            baseTree[0].material.backFaceCulling = false;
            baseTree[0].isVisible = false;
            baseTree[0].position.y = ground.getHeightAtCoordinates(0, 0); // Getting height from ground object

            if(treesBase !== null) {
                for (let index = 0; index < treesBase.length; index++) {
                    let newTreeInstance = baseTree[0].createInstance("tree" + index);
                    newTreeInstance.position = new BABYLON.Vector3(treesBase[index].x, treesBase[index].y, treesBase[index].z);
                    newTreeInstance.rotate(BABYLON.Axis.Y, Math.random() * Math.PI * 2, BABYLON.Space.WORLD);
                    newTreeInstance.scaling.addInPlace(new BABYLON.Vector3(treesBase[index].scale, treesBase[index].scale, treesBase[index].scale));
                    trees.push(newTreeInstance)
                }
                callback(trees);
                return;
            }

            for (let index = 0; index < countTree; index++) {
                let newTreeInstance = baseTree[0].createInstance("tree" + index);
                let x = range / 2 - Math.random() * range;
                let z = range / 2 - Math.random() * range;
                let y = ground.getHeightAtCoordinates(x, z) - 0.5; // Getting height from ground object

                newTreeInstance.position = new BABYLON.Vector3(x, y, z);
                newTreeInstance.rotate(BABYLON.Axis.Y, Math.random() * Math.PI * 2, BABYLON.Space.WORLD);

                let scaleRandom = function () {
                    return 25 + Math.random() * 100;
                };
                let scale = scaleRandom();
                newTreeInstance.scaling.addInPlace(new BABYLON.Vector3(scale, scale, scale));

                trees.push(newTreeInstance)
                treejson.push({
                    x: newTreeInstance.position.x,
                    y: newTreeInstance.position.y,
                    z: newTreeInstance.position.z,
                    scale: scale
                })
            }

            console.log(JSON.stringify(treejson));
            callback(trees);
        });
    },
    createBushs: function (ground, range, countBushs, callback) {
        let quality = 5;

        let treesBase = [{"x":-76.25324129735839,"y":-0.02524448849549671,"z":-70.89373445663517,"scale":94.01736512603904},{"x":-82.84536477780887,"y":5.1660472439379355,"z":30.234358434643326,"scale":113.45730377079346},{"x":109.56064966635225,"y":2.428704694673523,"z":36.42405419117895,"scale":76.55787417860394},{"x":-120.08902258492657,"y":-0.5,"z":-96.97253879579492,"scale":66.09750831936587},{"x":55.505083923811554,"y":-0.24093350685660347,"z":143.37872617970868,"scale":30.18547536503011},{"x":104.92983827978108,"y":-0.5,"z":-9.959672852145957,"scale":57.96678562400144},{"x":-88.44009026153083,"y":3.4262546534364415,"z":46.01991963742047,"scale":86.24055589592288},{"x":46.28172498845791,"y":7.510110743447916,"z":81.47774959240242,"scale":65.16366816128551},{"x":111.73386430486298,"y":2.87046869949402,"z":-72.91445624652141,"scale":116.41946475411586},{"x":32.41480624911705,"y":5.64049347991429,"z":78.58258373781437,"scale":122.6343506885934},{"x":-12.497351344873863,"y":0.009803921568627416,"z":-74.82768769940861,"scale":56.193777412603275},{"x":-22.083542142561498,"y":4.598039215686273,"z":-6.3998703554082965,"scale":55.385003929962195},{"x":6.589574866792248,"y":4.180528521388102,"z":87.09665508720792,"scale":50.08583393206343},{"x":-8.927820936944357,"y":3.6568627450980387,"z":81.85601269351379,"scale":75.87567239556148},{"x":-50.6184038598455,"y":-0.5,"z":-103.12024340242326,"scale":42.95939342627457},{"x":-39.33396648528023,"y":-0.5,"z":-137.31066245819648,"scale":66.16732536517523},{"x":-138.74078613384432,"y":-0.5,"z":-22.289373168205543,"scale":54.37809901415764},{"x":-89.45992503991374,"y":2.292354685817226,"z":71.22599719668155,"scale":41.62310303418806},{"x":62.95244776887033,"y":0.009803921568627416,"z":-68.21500737944564,"scale":75.67461076824534},{"x":-117.00794091590376,"y":0.06816033828017043,"z":-31.64316323367504,"scale":32.98095440618407},{"x":-21.80589259854645,"y":8.049013040989628,"z":62.1240092389709,"scale":50.6102179062965},{"x":76.98698973668603,"y":3.224463679739594,"z":60.96091733005393,"scale":56.857511622895714},{"x":-62.68001416720449,"y":2.281173137128355,"z":130.7578698825802,"scale":54.270298508054005},{"x":-35.688045461885054,"y":3.6568627450980387,"z":107.07414370503324,"scale":47.32106925985564},{"x":-12.693703657562168,"y":0.1989189465175044,"z":-68.37480203116803,"scale":88.94183002121599},{"x":136.5627487831266,"y":-0.5,"z":18.44823911911385,"scale":31.90271609578359},{"x":-94.56454826700048,"y":-0.5,"z":-133.9998793687775,"scale":112.01622723629796},{"x":-60.86870841637983,"y":6.189786340939429,"z":54.67303147328491,"scale":93.04512402666813},{"x":-57.61400495506831,"y":3.787961322577993,"z":110.00796893152207,"scale":123.42825509396438},{"x":-29.572275313535783,"y":3.9721374402379146,"z":89.39322287506451,"scale":71.81117801438302},{"x":-43.93600293057719,"y":7.628648292822062,"z":62.97545681072867,"scale":120.90844279420054},{"x":83.02376604378435,"y":-0.5,"z":142.6417355506315,"scale":27.892332341734495},{"x":-88.11316360498108,"y":0.08838386810918353,"z":-59.45661648893605,"scale":40.85823620553266},{"x":-102.59478543600488,"y":-0.5,"z":-131.7915580308794,"scale":39.80224953839924},{"x":47.02398442229918,"y":1.068627450980392,"z":-14.023303119540088,"scale":106.95390604340646},{"x":-110.89256086549744,"y":0.009803921568627416,"z":-58.12274501033622,"scale":62.765286808754595},{"x":-124.22463371984048,"y":-0.5,"z":-140.51719269735895,"scale":73.15545644390377},{"x":93.58979178957338,"y":1.1538414430420771,"z":17.741016150687898,"scale":90.7449127810558},{"x":34.510586264256276,"y":6.846491910078403,"z":119.95347169564148,"scale":26.713108219483715},{"x":2.9389665129319553,"y":-0.5,"z":-103.23907002290815,"scale":56.30022416342722},{"x":-133.8849017746042,"y":-0.5,"z":-107.02627207526712,"scale":53.43630594074102},{"x":-96.11095937190447,"y":2.4949668211050002,"z":-27.034773379836025,"scale":27.72316470306788},{"x":12.365287704141167,"y":0.009803921568627416,"z":-89.91107035418091,"scale":28.35512695393072},{"x":56.156625863966966,"y":2.6756569014658838,"z":4.413587685134701,"scale":108.90442263703903},{"x":-147.80272071086256,"y":-0.5,"z":66.10724563788139,"scale":72.45644700743466},{"x":-35.72261052015958,"y":-0.5,"z":-103.91447241419712,"scale":68.08798858282898},{"x":48.4390504061619,"y":0.009803921568627416,"z":-39.53821417153367,"scale":60.84950008611323},{"x":-85.7568875904264,"y":0.020177793500451346,"z":119.85702247052528,"scale":29.896865744107927},{"x":102.53820668669823,"y":0.014609254010851891,"z":16.081690651517818,"scale":65.71913166670328},{"x":-24.749911785071788,"y":6.805394141731314,"z":72.10955331171363,"scale":37.77516310607545}];
        let bushs = [];
        let bushsjson = [];

        let bush = BABYLON.Mesh.CreateSphere("bush", 16, 0.005, this.scene);
        bush.position.y = 15;
        let furMaterial = new BABYLON.FurMaterial("fur", this.scene);
        furMaterial.furLength = 0.2;
        furMaterial.furAngle = 0;
        furMaterial.furColor = new BABYLON.Color3(1, 1, 1);
        furMaterial.diffuseTexture = new BABYLON.Texture("texture/groundMaterial/grass.jpg", this.scene);
        furMaterial.furTexture = BABYLON.FurMaterial.GenerateTexture("furTexture", this.scene);
        furMaterial.furSpacing = 1;
        furMaterial.furDensity = 0.5;
        furMaterial.furSpeed = 17000;
        furMaterial.furGravity = new BABYLON.Vector3(0, -1, 0);

        bush.material = furMaterial;

        // Furify the sphere to create the high level fur effect
        // The first argument is sphere itself. The second represents
        // the quality of the effect
        BABYLON.FurMaterial.FurifyMesh(bush, quality);

        for (let index = 0; index < countBushs; index++) {
            let newBushInstance = bush.clone("bush" + index);
            let x = range / 2 - Math.random() * range;
            let z = range / 2 - Math.random() * range;
            let y = ground.getHeightAtCoordinates(x, z) - 0.3; // Getting height from ground object

            newBushInstance.position = new BABYLON.Vector3(x, y, z);
            newBushInstance.rotate(BABYLON.Axis.Y, Math.random() * Math.PI * 2, BABYLON.Space.WORLD);

            let scaleRandom = function () {
                return (Math.random() * 8) - 6;
            };

            let scale = scaleRandom();
            newBushInstance.scaling.addInPlace(new BABYLON.Vector3(scale+Math.random(), scale+Math.random(), scale+Math.random()));
            bushs.push(newBushInstance)
            bushsjson.push({
                x: newBushInstance.position.x,
                y: newBushInstance.position.y,
                z: newBushInstance.position.z,
                scale: scale
            })
        }

        bush.dispose();

        callback(bushs);
    },
    bot: function (bot, camera, ground) {

        var botX = bot.position.x;
        var botZ = bot.position.z;
        var camX = camera.position.x;
        var camZ = camera.position.z;
        var diffX = botX - camX;
        var diffZ = botZ - camZ;

        for (var cd = 0; cd < objectColision.length; cd++) {
            if (bot.intersectsMesh(objectColision[cd], true)) {
                bot.dispose();
            }
        }

        if (diffX < -0.1 || diffX > 0.1) {
            if (camX >= botX) {
                bot.position.x += 0.05;
                bot.position.y = ground.getHeightAtCoordinates(bot.position.x, bot.position.z);
                bot.rotation.y = -Math.PI / 2;
            } else {
                bot.position.x -= 0.05;
                bot.position.y = ground.getHeightAtCoordinates(bot.position.x, bot.position.z);
                bot.rotation.y = Math.PI / 2;
            }
        } else {
            if (diffZ != 0) {
                if (camZ >= botZ) {
                    bot.position.z += 0.05;
                    bot.position.y = ground.getHeightAtCoordinates(bot.position.x, bot.position.z);
                    bot.rotation.y = Math.PI;
                } else {
                    bot.position.z -= 0.05;
                    bot.position.y = ground.getHeightAtCoordinates(bot.position.x, bot.position.z);
                    bot.rotation.y = -2 * Math.PI;
                }
            }
        }
    },
    save: function (ground) {
        var buffer = new WORLDMONGER.Ground(ground);
        buffer.Save();
        var vb = buffer.VerticesPosition;
        var nm = buffer.VerticesNormal;

        $.ajax({url: "build/php/save_terrain.php", type: 'post', data: "vb=" + vb + "&nm=" + nm});

        alert("terrain sauvé");

    }
};