Weapons = function(Player) {
    this.Player = Player;

    this.Armory = Player.game.armory;

    this.bottomPosition = new BABYLON.Vector3(0.5,-2.5,1);

    // Changement de Y quand l'arme est séléctionné
    this.topPositionY = -0.5;

    this.inventory = [];

    this.tempAmmosBag = [];

    var crook = this.newWeapon('Crook');
    this.inventory[0] = crook;

	var armagedon = this.newWeapon('Armageddon');
	this.inventory[2] = armagedon;

	var timmy = this.newWeapon('Timmy');
	this.inventory[1] = timmy;

	var ezekiel = this.newWeapon('Ezekiel');
	this.inventory[3] = ezekiel;

    this.actualWeapon = this.inventory.length -1;

    this.inventory[this.actualWeapon].isActive = true;

    // voir typeWeapon
    this.fireRate = this.Armory.weapons[this.inventory[this.actualWeapon].typeWeapon].setup.cadency;

    this._deltaFireRate = this.fireRate;

    this.canFire = true;

    this.launchBullets = false;

    var _this = this;

    var engine = Player.game.scene.getEngine();

    this.textAmmos = document.getElementById('numberAmmos');

    this.totalTextAmmos = document.getElementById('totalAmmos');

    this.typeTextWeapon = document.getElementById('typeWeapon');

    var paramsActualWeapon = this.Armory.weapons[this.inventory[this.actualWeapon].typeWeapon];

    if(paramsActualWeapon.setup.ammos){
        this.textAmmos.innerText = this.inventory[this.actualWeapon].ammos;
        this.totalTextAmmos.innerText = paramsActualWeapon.setup.ammos.maximum;
        this.typeTextWeapon.innerText = paramsActualWeapon.name;
    }

    // temps de rechargement
    this._animationDelta = 0;

    Player.game.scene.registerBeforeRender(function() {
        if (!_this.canFire) {

            // Arme chagéz ou n'a pas de munitions
            if(_this.inventory[_this.actualWeapon].ammos == undefined || (
                    _this.inventory[_this.actualWeapon].ammos &&
                    _this.inventory[_this.actualWeapon].ammos>0)){
                // Animation
                _this.animateMovementWeapon(_this._animationDelta);
            }

            // animationDelta
            _this._animationDelta += engine.getDeltaTime();
            _this._deltaFireRate -= engine.getDeltaTime();

            if (_this._deltaFireRate <= 0 && _this.Player.isAlive) {
                // fin animation on replace l'arme a sa position de base
                _this.inventory[_this.actualWeapon].position =
                    _this.inventory[_this.actualWeapon].basePosition.clone();
                _this.inventory[_this.actualWeapon].rotation =
                    _this.inventory[_this.actualWeapon].baseRotation.clone();

                _this.canFire = true;
                _this._deltaFireRate = _this.fireRate;

                // Quand on peut tirer on repasse animationDelta à 0
                _this._animationDelta = 0;
            }
        }
    });

};

Weapons.prototype = {
    newWeapon : function(typeWeapon) {
        var newWeapon;
        for (var i = 0; i < this.Armory.weapons.length; i++) {
            if(this.Armory.weapons[i].name === typeWeapon){

                newWeapon = BABYLON.Mesh.CreateBox('weaponHand', 0.5, this.Player.game.scene);

                newWeapon.scaling = new BABYLON.Vector3(1,0.7,2);

                newWeapon.parent = this.Player.camera;

                newWeapon.position = this.bottomPosition.clone();

                newWeapon.isPickable = false;

                var materialWeapon = new BABYLON.StandardMaterial('rocketLauncherMat', this.Player.game.scene);
                materialWeapon.diffuseColor=this.Armory.weapons[i].setup.colorMesh;

                newWeapon.material = materialWeapon;

                newWeapon.typeWeapon = i;

                newWeapon.isActive = false;

                if(this.Armory.weapons[i].setup.ammos){
                    newWeapon.ammos = this.Armory.weapons[i].setup.ammos.baseAmmos;

                    if(this.tempAmmosBag[i]){
                        newWeapon.ammos += this.tempAmmosBag[i];

                        if(newWeapon.ammos > this.Armory.weapons[i].setup.ammos.maximum){
                            newWeapon.ammos = this.Armory.weapons[i].setup.ammos.maximum;
                        }
                    }
                }

                newWeapon.basePosition = newWeapon.position;
                newWeapon.baseRotation = newWeapon.rotation;
                break;
            }else if(i === this.Armory.weapons.length -1){
                console.log('Arme inconnue');
            }
        };

        return newWeapon
    },
    fire : function(pickInfo) {
        this.launchBullets = true;
    },
    stopFire : function(pickInfo) {
        this.launchBullets = false;
    },
    launchFire : function() {
        if (this.canFire) {
            // Id de l'arme en main
            var idWeapon = this.inventory[this.actualWeapon].typeWeapon;

            var renderWidth = document.getElementById('renderCanvas').offsetWidth;
            var renderHeight = document.getElementById('renderCanvas').offsetHeight;

            var weaponAmmos = this.inventory[this.actualWeapon].ammos;

            // rayon au centre de l'écran
            var direction = this.Player.game.scene.pick(renderWidth/2,renderHeight/2,function (item) {
                if (item.name == "weaponHand" || item.id == "headMainPlayer" || item.id == "hitBoxPlayer" || item.id == "camera")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            });

            if(this.Armory.weapons[idWeapon].type === 'ranged'){
                if(weaponAmmos>0){
                    if(this.Armory.weapons[idWeapon].setup.ammos.type === 'rocket'){

                        direction = direction.pickedPoint.subtractInPlace(this.inventory[this.actualWeapon].absolutePosition.clone());
                        direction = direction.normalize();
                        // console.log(direction)
                        this.createRocket(this.Player.camera.playerBox,direction);
                    }else if(this.Armory.weapons[idWeapon].setup.ammos.type === 'bullet'){
                        this.shootBullet(direction)
                    }else{
                        this.createLaser(direction)
                    }
                    this.inventory[this.actualWeapon].ammos--;
                    this.textAmmos.innerText = this.inventory[this.actualWeapon].ammos;
                }
            }else{
                this.hitHand(direction)
            }
            this.canFire = false;
        } else {
        }
    },
    createRocket : function(playerPosition, direction) {
        var positionValue = this.inventory[this.actualWeapon].absolutePosition.clone();
        // console.log(this.inventory[this.actualWeapon].absolutePosition.clone())
        var rotationValue = playerPosition.rotation;
        var Player = this.Player;
        var newRocket = BABYLON.Mesh.CreateBox("rocket", 1, Player.game.scene);

        var idWeapon = this.inventory[this.actualWeapon].typeWeapon;

        var setupRocket = this.Armory.weapons[idWeapon].setup.ammos;

        newRocket.direction = direction;

        console.log(direction);

        newRocket.position = new BABYLON.Vector3(
            positionValue.x + (newRocket.direction.x * 1) ,
            positionValue.y + (newRocket.direction.y * 1) ,
            positionValue.z + (newRocket.direction.z * 1));
        newRocket.rotation = new BABYLON.Vector3(rotationValue.x,rotationValue.y,rotationValue.z);
        newRocket.scaling = new BABYLON.Vector3(0.5,0.5,1);

        newRocket.material = new BABYLON.StandardMaterial("textureWeapon", this.Player.game.scene);

        newRocket.material.diffuseColor = this.Armory.weapons[idWeapon].setup.colorMesh;
        newRocket.paramsRocket = this.Armory.weapons[idWeapon].setup;

        newRocket.isPickable = false;

        sendGhostRocket(newRocket.position,newRocket.rotation,newRocket.direction);

        this.Player.game._rockets.push(newRocket);
    },
    shootBullet : function(meshFound) {
        var setupWeapon = this.Armory.weapons[this.actualWeapon].setup;

        var idWeapon = this.inventory[this.actualWeapon].typeWeapon;

        if(meshFound.hit && meshFound.pickedMesh.isPlayer){
            var damages = this.Armory.weapons[idWeapon].setup.damage;
            sendDamages(damages,meshFound.pickedMesh.name)
        }else{
            console.log('No pickedMesh')
        }
    },
    createLaser : function(meshFound) {
        var setupLaser = this.Armory.weapons[this.actualWeapon].setup.ammos;

        var positionValue = this.inventory[this.actualWeapon].absolutePosition.clone();

        var idWeapon = this.inventory[this.actualWeapon].typeWeapon;

        if(meshFound.hit){

            var laserPosition = positionValue;
            let line = BABYLON.Mesh.CreateLines("lines", [
                laserPosition,
                meshFound.pickedPoint
            ], this.Player.game.scene);

            var colorLine = new BABYLON.Color3(Math.random(), Math.random(), Math.random());
            line.color = colorLine;

            line.enableEdgesRendering();
            line.isPickable = false;
            line.edgesWidth = 40.0;
            line.edgesColor = new BABYLON.Color4(colorLine.r, colorLine.g, colorLine.b, 1);
            if(meshFound.pickedMesh.isPlayer){
                var damages = this.Armory.weapons[idWeapon].setup.damage;
                sendDamages(damages,meshFound.pickedMesh.name)
            }

            sendGhostLaser(laserPosition,meshFound.pickedPoint);

            this.Player.game._lasers.push(line);
        }
    },
    hitHand : function(meshFound) {
        var idWeapon = this.inventory[this.actualWeapon].typeWeapon;

        var setupWeapon = this.Armory.weapons[idWeapon].setup;
        if(meshFound.hit && meshFound.distance < setupWeapon.range*5 && meshFound.pickedMesh.isPlayer){
            var damages = this.Armory.weapons[idWeapon].setup.damage;
            sendDamages(damages,meshFound.pickedMesh.name)
        }else{
            console.log('Not Hit CaC')
        }
    },
    nextWeapon : function(way) {
        var armoryWeapons = this.Armory.weapons;

        var nextWeapon = this.inventory[this.actualWeapon].typeWeapon + way;

        var nextPossibleWeapon = null;

        if(way>0){
            for (var i = nextWeapon; i < nextWeapon + this.Armory.weapons.length; i++) {
                var numberWeapon = i % this.Armory.weapons.length;
                for (var y = 0; y < this.inventory.length; y++) {
                    if(this.inventory[y].typeWeapon === numberWeapon){
                        nextPossibleWeapon = y;
                        break;
                    }
                }
                if(nextPossibleWeapon != null){
                    break;
                }
            }
        }else{
            for (var i = nextWeapon; ; i--) {
                if(i<0){
                    i = this.Armory.weapons.length;
                }
                var numberWeapon = i;
                for (var y = 0; y < this.inventory.length; y++) {
                    if(this.inventory[y].typeWeapon === numberWeapon){
                        nextPossibleWeapon = y;
                        break;
                    }
                }
                if(nextPossibleWeapon != null){
                    break;
                }
            }
        }
        if(this.actualWeapon != nextPossibleWeapon){

            this.inventory[this.actualWeapon].position =
                this.inventory[this.actualWeapon].basePosition.clone();

            this.inventory[this.actualWeapon].rotation =
                this.inventory[this.actualWeapon].baseRotation.clone();

            this._animationDelta = 0;

            this.inventory[this.actualWeapon].isActive = false;
            this.inventory[this.actualWeapon]
            this.actualWeapon = nextPossibleWeapon;
            this.inventory[this.actualWeapon].isActive = true;

            this.fireRate = this.Armory.weapons[this.inventory[this.actualWeapon].typeWeapon].setup.cadency;
            this._deltaFireRate = this.fireRate;
            var actualTypeWeapon = this.Armory.weapons[this.inventory[this.actualWeapon].typeWeapon];

            if(actualTypeWeapon.setup.ammos){
                this.textAmmos.innerText = this.inventory[this.actualWeapon].ammos;
                this.totalTextAmmos.innerText = actualTypeWeapon.setup.ammos.maximum;
                this.typeTextWeapon.innerText = actualTypeWeapon.name;
            }else{
                this.typeTextWeapon.innerText = actualTypeWeapon.name;
                this.textAmmos.innerText = "Inf";
                this.totalTextAmmos.innerText = "Inf";
            }
        }
    },
    animateMovementWeapon : function(step){
        if(!this.Player.isAlive){
            return;
        }
        let typeWeapon = this.inventory[this.actualWeapon].typeWeapon;
        let result = (step / this.Armory.weapons[typeWeapon].timeAnimation) * 180;

        if(result>180){
            result = 180;
        }

        // arrondie avec 100
        let degSin = Math.round(Math.sin(degToRad(result))*100)/100;



        // TODO Position de l'arme lors d'un rajout
        switch(typeWeapon){
            case 0:
                var positionNeeded = new BABYLON.Vector3(0,-0.5,0);
                var rotationNeeded = new BABYLON.Vector3(-0.5,0,0);
                break;
            case 1:
                var positionNeeded = new BABYLON.Vector3(0.05,0.05,0);
                var rotationNeeded = new BABYLON.Vector3(0.1,0.1,0);
                break;
            case 2:
                var positionNeeded = new BABYLON.Vector3(0,0.4,0);
                var rotationNeeded = new BABYLON.Vector3(1.3,0,0);
                break;
            case 3:
                var positionNeeded = new BABYLON.Vector3(0,0,-1);
                var rotationNeeded = new BABYLON.Vector3(0,0,0);
                break;
        }

        var baseRotation = this.inventory[this.actualWeapon].baseRotation.clone();
        var basePosition = this.inventory[this.actualWeapon].basePosition.clone();

        this.inventory[this.actualWeapon].rotation = baseRotation.clone() ;
        this.inventory[this.actualWeapon].rotation.x -= (rotationNeeded.x*degSin);

        this.inventory[this.actualWeapon].position = basePosition.clone() ;
        this.inventory[this.actualWeapon].position.y += (positionNeeded.y*degSin);
        this.inventory[this.actualWeapon].position.z += (positionNeeded.z*degSin);
    },
    reloadWeapon : function(type,numberAmmos) {
        var ammoHud = document.getElementById('ammosValue');
        var existingWeapon = false;

        for (var i = 0; i < this.inventory.length; i++) {
            if(this.inventory[i].typeWeapon === type){

                var existingWeapon = true;
                if((this.inventory[i].ammos + numberAmmos) >
                    this.Armory.weapons[i].setup.ammos.maximum){
                    this.inventory[i].ammos = this.Armory.weapons[i].setup.ammos.maximum
                }else{
                    this.inventory[i].ammos += numberAmmos;
                }
                var actualTypeWeapon = this.Armory.weapons[this.inventory[this.actualWeapon].typeWeapon];
                if(this.inventory[this.actualWeapon].typeWeapon === type){
                    this.textAmmos.innerText = this.inventory[this.actualWeapon].ammos;
                    this.totalTextAmmos.innerText = actualTypeWeapon.setup.ammos.maximum;
                }

                break;
            }
        }

        if(!existingWeapon){

            if(!this.tempAmmosBag[type]){
                this.tempAmmosBag[type]=0;
            }
            if((this.tempAmmosBag[type] + numberAmmos) >
                this.Armory.weapons[type].setup.ammos.maximum){
                this.tempAmmosBag[type] = this.Armory.weapons[type].setup.ammos.maximum;
            }else{
                this.tempAmmosBag[type] += numberAmmos;
            }
        }
    },
};