// ================================================
// DECLARATION SOCKET
var socket = io();
// ================================================

// ================================================
// MAIN
var game;
var myRoom = [];
var personalRoomId = false;
var myConfig = {};
var isPlayerAlreadySet = false;

socket.on('newPlayer',function(dataNewPlayer){
    var room = dataNewPlayer[0];
    var score = dataNewPlayer[1];
    var props = dataNewPlayer[2];
    if(!isPlayerAlreadySet){
        for(var i=0;i<room.length;i++){     
            if(room[i].id == socket.id){
                myConfig = room[i];
                personalRoomId = room[i].id;
                game = new Game('renderCanvas',myConfig,props);
                isPlayerAlreadySet = true;
                document.getElementById('gameName').innerText = room[i].name;
                myConfig.name = room[i].name;
            }
        }
    }

    game.displayScore(score);

    // check connexion
    checkIfNewGhost(room);
});

socket.on('disconnectPlayer', function(room){
    checkIfGhostDisconnect(room);
});

	//On charge la map importée de mysql pour le joueur
	demandeMap();

var sortRoom = function(room){
    return room.sort(function(a, b) {
            var nameA = a.id.toUpperCase();
            var nameB = b.id.toUpperCase();
            if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        return 0;
    });
}
var checkIfNewGhost = function(room){
    for(var i=0;i<room.length;i++){
        if(room[i].id != personalRoomId){
            var ghostAlreadyExist = false;
            for(var j=0;j<myRoom.length;j++){
                if(room[i].id == myRoom[j].id){
                    ghostAlreadyExist = true;
                    break;
                }
            }
            if(!ghostAlreadyExist){
                createGhost(room[i],room[i].id);
            }
        }
    }
}
var checkIfGhostDisconnect = function(room){
    for(var i=0;i<myRoom.length;i++){
        var ghostExist = false;
        for(var j=0;j<room.length;j++){
            if(myRoom[i].id == room[j].id){
                ghostExist = true;
                break;
            }
        }

        if(!ghostExist){
            deleteGhost(myRoom[i].id,i);
        }
    }
}
var createGhost = function(ghost,id){
    myRoom.push(ghost);
    newGhostPlayer = GhostPlayer(game,ghost,id);
    game._PlayerData.ghostPlayers.push(newGhostPlayer);
}
var updateGhost = function(data){
    socket.emit('updateData',[data,personalRoomId]);
}
function demandeMap(){
    socket.emit('demandeMap');
}
var deleteGhost = function(index,position){
    deleteGameGhost(game,index);
    myRoom.splice(position,1);
}
var sendGhostRocket = function(position, rotation, direction){
    socket.emit('newRocket',[position, rotation, direction, personalRoomId]);
}
var sendGhostLaser = function(position1, position2){
    socket.emit('newLaser',[position1, position2, personalRoomId]);
}
var sendDamages = function(damage,target){
    socket.emit('distributeDamage',[damage, target, personalRoomId]);
}
var sendPostMortem = function(whoKilledMe){
    if(!whoKilledMe){
        var whoKilledMe = personalRoomId;
    }
    socket.emit('killPlayer',[personalRoomId,whoKilledMe]);
}
var ressurectMe = function(position){
    var dataToSend = [game._PlayerData.sendActualData(),personalRoomId];
    dataToSend[0].ghostCreationNeeded = true;
    socket.emit('updateData',dataToSend);
}

var destroyPropsToServer = function(idServer,type){
    socket.emit('updatePropsRemove',[idServer,type]);
}

socket.on('requestPosition', function(room){
    var dataToSend = [game._PlayerData.sendActualData(),personalRoomId];
    socket.emit('updateData',dataToSend);
});

socket.on ('updatePlayer', function (arrayData) {
    if(arrayData.id != personalRoomId){
        if(arrayData.ghostCreationNeeded){
            var newGhostPlayer = GhostPlayer(game,arrayData,arrayData.id);
            game._PlayerData.ghostPlayers.push(newGhostPlayer);
        }else{
            game._PlayerData.updateLocalGhost(arrayData);
        }
    }
});

socket.on ('createGhostRocket', function (arrayData) {
    if(arrayData[3] != personalRoomId){
        game.createGhostRocket(arrayData);
    }
});

socket.on ('createGhostLaser', function (arrayData) {
    if(arrayData[2] != personalRoomId){
        game.createGhostLaser(arrayData);
    }
});

socket.on ('giveDamage', function (arrayData) {
    if(arrayData[1] == personalRoomId){
        game._PlayerData.getDamage(arrayData[0],arrayData[2]);
    }
    
});
socket.on ('killGhostPlayer', function (arrayData) {
    var idArray = arrayData[0];
    var roomScore = arrayData[1];
    if(idArray[0] != personalRoomId){
        deleteGameGhost(game,idArray[0]);
    }
    if(idArray[1] == personalRoomId){
        game._PlayerData.newDeadEnnemy(idArray[2]);
    }
    game.displayScore(roomScore);
});
socket.on ('ressurectGhostPlayer', function (idPlayer) {
    if(idPlayer != personalRoomId){
        deleteGameGhost(game,idPlayer);
    }
});

socket.on ('creerMap', function (map, name) {
    game._ArenaData.creerMap(map, name)
});

socket.on ('deleteProps', function (deleteProp) {
    game._ArenaData.deletePropFromServer(deleteProp)
});
socket.on ('recreateProps', function (createdProp) {
    game._ArenaData.recreatePropFromServer(createdProp)
});
