var express = require('express')
  , http = require('http'),
	mysql = require("mysql"),
	colors = require('colors');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var os = require('os');
var ifaces = os.networkInterfaces();

var exec = require("child_process").exec;

// Effacer la console
process.stdout.write('\033c');


// MYSQL
// var con = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "",
//   database: "map"
// });


// con.connect(function(err){
//  if(err){
//    console.log('Impossible de se connecter à la DB'.red);
//    return;
//  }
//
//   console.log('Connexion à la DB reussie'.green);
// });



// Informations serveur
console.log(colors.yellow.underline('SERVEUR EN ECOUTE'));
Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
        
        return;
    }
    if (alias >= 1) {
        // plusieurs ipv4
        console.log(ifname + ':' + alias, iface.address);
    } else {
        // une ipv4
		
      //console.log(ifname, iface.address);
		console.log(colors.white('IP : ') + colors.green('%s / ') + colors.white('PORT : ') + colors.green('3000'), iface.address);

    }
    ++alias;
    });
});
console.log('=============');

app.use(express.static(__dirname + '/fps'));
app.use('/static', express.static(__dirname + '/fps'));

var room = [];
var sockets = {};
var namesList = [
        "Alpha",
        "Bravo",
        "Charlie",
        "Delta",
        "Echo",
        "Foxtrot",
        "Golf",
        "Hotel",
        "India",
        "Juliett",
        "Kilo",
        "Lima",
        "Mike",
        "November",
        "Oscar",
        "Papa",
        "Québec",
        "Romeo",
        "Sierra",
        "Tango",
        "Uniform",
        "Victor",
        "Whisky",
        "X-ray",
        "Yankee",
        "Zulu"];
		
var spawnPointsList = [
    {x:-20, y:5, z:0},
    {x:0, y:5, z:0},
    {x:20, y:5, z:0},
    {x:40, y:5, z:0}
];

var bonusBoxes = [
    {x:33, y:10,z:-20,t:2,v:1},
    {x:-33, y:10,z:-20,t:0,v:1},
    {x:15, y:10,z:155,t:1,v:1},
    {x:-15, y:10,z:155,t:1,v:1}
];
var weaponBoxes = [
    {x: 59, y:10,z:-55,t:3,v:1},
    {x:-8, y:10,z: 44.7,t:2,v:1}
];
var ammosBoxes = [
    {x:15, y:10,z:173,t:2,v:1},
    {x:-15, y:10,z:173,t:2,v:1},
    {x:15, y:10,z:85,t:1,v:1},
    {x:-15, y:10,z:85,t:1,v:1},
    {x:-33, y:10,z:40,t:3,v:1},
    {x:33, y:10,z:40,t:1,v:1}
];

var props = [bonusBoxes,weaponBoxes,ammosBoxes]
var countUsers = 0;



io.on('connection', function(socket){
    var name = getAName();
    countUsers++;
    socket.name = name;
	
    var tempUser = {
        id: socket.client.id,
        name: socket.name,
        life: 100,
        armor: 0,
        jumpNeed: false,
        position: getSpawnPoint(),
        actualTypeWeapon: 1,
        axisMovement: [false,false,false,false],
        rotation: {x:0, y:0, z:0},
        score: 0
    };
    room.push(tempUser);

    io.emit('newPlayer',[room,getTopFive(room),props]);
	console.log(colors.red('\n==================================='));
	console.log(name + ' viens de se connecter'.white);


	socket.on('demandeMap', function() {
        // Chargement de la map
		// con.query('SELECT normal,position FROM terrain ORDER BY id DESC LIMIT 1',  [] ,function(err,rows){
		// 	if(err) throw err;
        //
        //
		// 	var position = rows[0].position;
		// 	var normal = rows[0].normal;
        //
        //     position = position.toString('utf8');
        //     normal = normal.toString('utf8');
        //
        //     io.emit('creerMap', [position , normal], name);
		// 	console.log('Creation de la map pour ' + name + ''.white);
		// 	console.log(colors.red('==================================='));
        //
		// });
    });
	
    socket.on('disconnect', function() {
        for(var i=0;i<room.length;i++){
            if(room[i].id === socket.client.id){
                room.splice(i, 1);
                io.emit('disconnectPlayer',room);
				console.log('\n' + name + ' s\'est deconnecté'.white);

            }
        }
    });
    socket.on('newRocket', function(data) {
        io.sockets.emit ('createGhostRocket', data);
    });
    socket.on('newLaser', function(data) {
        io.sockets.emit ('createGhostLaser', data);
    });
    socket.on('distributeDamage', function(data) {
        io.sockets.emit ('giveDamage', data);
    });
    socket.on('killPlayer', function(arrayData) {
        var idPlayer = arrayData[0];
        var idKiller = arrayData[1];
        for(var i=0;i<room.length;i++){ 
            if(room[i].id === idKiller){
                if(idKiller === idPlayer){
                    room[i].score--;
                }else{
                    room[i].score++;
                }
            }
            if(room[i].id === idPlayer){
                arrayData[2] = room[i].name;
            }
        }
        io.sockets.emit ('killGhostPlayer', [arrayData,room]);
    });
    socket.on('ressurectPlayer', function(idPlayer) {
        io.sockets.emit ('ressurectGhostPlayer', idPlayer);
    });
    socket.on('updateData', function(arrayData) {
        var idPlayer = arrayData[1];
        var data = arrayData[0];
        for(var i=0;i<room.length;i++){  
            if(room[i].id === idPlayer){
                var datasend = {};
                if(data.position){
                    room[i].position.x = data.position.x;
                    room[i].position.y = data.position.y;
                    room[i].position.z = data.position.z;
                    datasend.position = room[i].position;
                }
                if(data.axisMovement){
                    room[i].axisMovement = data.axisMovement;
                    datasend.axisMovement = room[i].axisMovement;
                }
                if(data.rotation){
                    // console.log(data.rotation)
                    room[i].rotation.x = data.rotation.x;
                    room[i].rotation.y = data.rotation.y;
                    room[i].rotation.z = data.rotation.z;
                    datasend.rotation = room[i].rotation;
                }
                if(data.armor){
                    room[i].armor = data.armor;
                    room[i].health = data.health;
                    datasend.armor = room[i].armor;
                    datasend.health = room[i].health;
                }
                if(data.actualWeapon){
                    room[i].actualWeapon = data.actualWeapon;
                    datasend.actualWeapon = room[i].actualWeapon;
                }
                if(data.jumpNeed){
                    room[i].jumpNeed = data.jumpNeed;
                    datasend.jumpNeed = room[i].jumpNeed;
                }
                if(data.ghostCreationNeeded){
                    datasend.ghostCreationNeeded = true;
                }
                datasend.id = room[i].id;
                io.sockets.emit ('updatePlayer', datasend);
                break;
            }
        }
    });
    socket.on('updatePropsRemove', function(dataRemove) {
        var idServer = dataRemove[0];
        var type = dataRemove[1];
        io.sockets.emit ('deleteProps', dataRemove);
        switch (type){
            case 'ammos' :
                ammosBoxes[idServer].v = 0;
                launchCountDownRepop(2000,dataRemove);
            break;
            case 'bonus' :
                bonusBoxes[idServer].v = 0;
                launchCountDownRepop(2000,dataRemove);
            break;
            case 'weapon' :
				console.log("Une arme a été pick");
                weaponBoxes[idServer].v = 0;
                launchCountDownRepop(2000,dataRemove);
            break;
        }
    });	
});



var getAName = function(){
    return namesList[(countUsers % namesList.length)] + (countUsers/namesList.length|0!=0 ? (countUsers/namesList.length|0)+1 : "");
}

var getSpawnPoint = function(){
    return spawnPointsList[countUsers % spawnPointsList.length];
}

var getTopFive = function(room){
    var score = room;
    function compare(a,b) {
        if (a.score < b.score)
            return -1;
        if (a.score > b.score)
            return 1;
        return 0;
    }

    score.sort(compare);
    return score;
}

var launchCountDownRepop = function(time,dataRemoved){
    var dataRecreated = dataRemoved;
    setTimeout(function() {
        io.emit('recreateProps',dataRecreated);
        switch (dataRecreated[1]){
            case 'ammos' :
                ammosBoxes[dataRecreated[0]].v = 1;
            break;
            case 'bonus' :
                bonusBoxes[dataRecreated[0]].v = 1;
            break;
            case 'weapon' :
                weaponBoxes[dataRecreated[0]].v = 1;
            break;
        }
    }, time);
}

setInterval(function(){
    io.emit('requestPosition',room);
}, 5000);


server.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

	console.log('En attente d\'evenement ... \n');

});